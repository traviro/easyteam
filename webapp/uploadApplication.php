<?php
// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.

// Upload form for theproject application
// Proper use is embedding it within the index.html page


if( isset( $_GET['ul'] ) ) {
	$allowedExts = array("pdf");
	$temp = explode(".", $_FILES["file"]["name"]);
	$extension = end($temp);
	if ($_FILES["file"]["type"] == "application/pdf"
		&& ($_FILES["file"]["size"] < 100000000 ) // 25 mb size limit 
		&& in_array($extension, $allowedExts))
			{
				if ($_FILES["file"]["error"] > 0)
				{
					echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
				}
				else
				{
					//echo "Upload: " . $_FILES["file"]["name"] . "<br>";
					//echo "Type: " . $_FILES["file"]["type"] . "<br>";
					//echo "Size: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
					//echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br>";

					if (file_exists("uploads/applications" . $_FILES["file"]["name"]))
					{
						javascript('window.top.bootbox.alert("Error: File already exists")');
					}
					else
					{
						move_uploaded_file($_FILES["file"]["tmp_name"],
						"uploads/applications/" . time() . $_FILES["file"]["name"]);

						$path = "uploads/applications/" . time() . $_FILES["file"]["name"];

						//echo "Stored in: " . "uploads/applications/" . time() . $_FILES["file"]["name"];
						javascript('window.top.createApplication("' . $path .'", "'.$_POST['ProjectID'].'")');
					}
				}
			}
	else
	{
		javascript('window.top.bootbox.alert("Error: Invalid file"); window.top.viewProject('.$_POST['ProjectID'].')');
	}
}
else {
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Upload Application</title>
		<meta charset="utf-8">
		<style>
			html, body {
				overflow-x: hidden;
			}
		</style>
	</head>
	<body>
		<form enctype="multipart/form-data" action="uploadApplication.php?ul=true" method="POST">
			<input type="hidden" name="ProjectID" value="<?php echo $_GET['ProjectID']; ?>" />
			<input type="hidden" name="MAX_FILE_SIZE" value="100000000" />
			<input name="file" type="file" accept="application/pdf" /><br/>
			<input type="submit" value="Upload File" />
		</form>
	</body>
</html>
<?php
	}


	function javascript($script) {

		echo '<!DOCTYPE html>';
		echo 	'<html>';
		echo 		'<head>';
		echo			'<title>Upload Application</title>';
		echo			'<meta charset="utf-8">';
		echo		'</head>';
		echo		'<body>';
		echo			'<script type="text/javascript">';
		echo 				$script;
		echo			'</script>';
		echo 		'</body>';
		echo 	'</html>';
	}
?>