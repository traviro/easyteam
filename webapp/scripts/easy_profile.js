// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.


// Populate the view profile page you need and then view it
function viewProfile(uid) {

	loadBig(true);
	getUser(uid, function(user) {
		loadBig(false);


		// First lookup AOF and Class based on the ID's in the user object
		var aof = "none";
		for(var i = 0; i < AreasOfFocus.length; i++) {
			if( user.AreaOfFocusID == AreasOfFocus[i].AreaOfFocusID ) {
				aof = AreasOfFocus[i].AreaOfFocusName;
				break;
			}
		}

		var uclass = {"ClassTerm" : "n/a", "ClassYear" : ""};
		// find the class term info
		for(var c = 0; c < Classes.length; c++ ) {
			if( Classes[c].ClassID == user.ClassID) {
				uclass = Classes[c];
				break;
			}
		}



		// Populate the fields with the right data
		$('#profileAOF').text(aof);
		$('#profileClass').text(uclass.ClassTerm + " " + uclass.ClassYear);

		var nametext = user.UserName;
		if( user.FirstName && user.LastName ) nametext = user.FirstName + ' ' + user.LastName;		

		$('#profileName').text( nametext );
		$('#profilePortfolio').text( user.PortfolioURL ).attr('href', user.PortfolioURL );
		$('#profileAboutMe').text( user.AboutMe );
		$('#profileProject').text('');
		$('#profileEmail').text( user.Email ).attr('href', 'mailto:'+user.Email );

		$('#profileBtn').text(user.FirstName);

		if(user.ProjectID) {
			getProject(user.ProjectID, function(project) {
				$('#profileProject')
					.html('<a href="#">'+project.ProjectName+'</a>')
					.click( function() {
						viewProject(project.ProjectID);
					});
			});
		}


		// Deselect all the nav links since this page isn't one of them
		$('.navlink').removeClass('active');

		// Navigate to the view profile page
		Navigate('View-Profile');
	});

}



function editMyProfile() {
	if(UserData.UserID) {
		populateEditProfile(UserData.UserID)
	}
}

function showEditProfile(uid) {
	$('.page').hide(100);
	$('#Edit-Profile').show(100);
	$('#secondaryCrumb').text('Edit Profile');

	populateEditProfile(uid)
}

// Populate the user's edit my profile info
function populateEditProfile(uid) {

	loadBig(true);
	getUser(uid, function(user) {
		loadBig(false);

		var nametext = user.UserName;
		if( user.FirstName && user.LastName ) nametext = user.FirstName + ' ' + user.LastName;
		// Let's set the stuff we already know
		$('#firstNameInput').val( user.FirstName );
		$('#lastNameInput').val( user.LastName );
		$('#aboutMeInput').val( user.AboutMe );
		$('#phoneNumberInput').val( user.PhoneNumber );
		$('#portfolioURLInput').val( user.PortfolioURL );
		$('#emailInput').val( user.Email );
		$('.firstName').text(nametext);

		// Clear old areas of focus
		$('#areaOfFocusInput').html('');

		// Add in options from server
		for(var i = 0; i < AreasOfFocus.length; i++ ) {
			$('<option/>')
				.attr("value", AreasOfFocus[i].AreaOfFocusID  )
				.text( AreasOfFocus[i].AreaOfFocusName )
				.appendTo( $('#areaOfFocusInput') );
		}

		// Set the val to the user's current choice
	 	$('#areaOfFocusInput').val( user.AreaOfFocusID );

		// Add in class term options
		$('#classTermInput').html('');
		for(var i = 0; i < Classes.length; i++ ) {
			$('<option/>')
				.attr("value", Classes[i].ClassID  )
				.text( Classes[i].ClassTerm + ' ' + Classes[i].ClassYear )
				.appendTo( $('#classTermInput') );
		}
		$('#classTermInput').val( user.ClassID );





		// Get the project list
		$('#sponsorForInput, #advocateForInput')
			.html('').append('<option value="NULL">None</option>').css('display','block');

		// hide the advocate or sponsor select box depending on user type
		if(user.UserType == 2 ) {
			$('#advocateForInput').css('display','none');
		}

		if(user.UserType == 1 ) {
			$('#sponsorForInput').css('display','none');
		}		

		$.ajax({
			"url"		: "../api/Project/getProjects.php",
			"dataType"	: "json",
			"success"	: function(data) {
				if(!data.Error) {
					for(var i =0; i < data.length; i++) {
						console.log(data[i]);
						$('#sponsorForInput, #advocateForInput')
							.append('<option value="' + data[i].ProjectID + '">'+ data[i].ProjectName+'</option>');

						if(data[i].SponsorID == user.UserID ) $('#sponsorForInput').val( data[i].ProjectID );
						if(data[i].AdvocateID == user.UserID ) $('#advocateForInput').val( data[i].ProjectID );

					}
				}
			}
		});


	 	// bind the mosue buttons click event
	 	$('#submitEditProfile').unbind("click").click(function() { editProfile(uid); });
	});
		

}



// Edit profile using the information given in the forms
function editProfile(uid) {

	var data = {};

	// get the data from the variables
	data.FirstName 		= $('#firstNameInput').val();
	data.LastName 		= $('#lastNameInput').val();
	data.AreaOfFocusID 	= $('#areaOfFocusInput').val();
	data.AboutMe 		= $('#aboutMeInput').val();
	data.PhoneNumber 	= $('#phoneNumberInput').val();
	data.PortfolioURL 	= $('#portfolioURLInput').val();
	data.Email 			= $('#emailInput').val();
	data.ClassID		= $('#classTermInput').val();
	data.UserID			= uid;

	var password1 = $('#passwordInput').val();
	var password2 = $('#password2Input').val();

	var passChange = false;


	// if the passwords match it's o.k.

	if( password1.length > 0 ) passChange = true;
	if( password1 == password2 ) {
		if(passChange) data.Password = password1;
	}
	else {
		bootbox.alert("Error: Passwords don't match.");
		return;
	}

	// Make the ajax call
	loadBig(true);
	$.ajax({
		"url"		: "../api/User/editUser.php",
		"dataType" 	: "json",
		"data"		: data,
		"success"	: function(data) {
			loadBig(false);
			if(!data.Error) {
				showEditProfile(uid);
				bootbox.alert("Profile updated successfully");
			}
			else {
				bootbox.alert("Error: " + data.Error);
			}
		}
	});



	var sponsorProject = $('#sponsorForInput').val();
	var advocateProject = $('#advocateForInput').val();

	if(sponsorProject != "NULL") {
		$.ajax({
			"url"	: '../api/Project/editProject.php',
			'dataType' : 'json',
			'data' 	: {
				'ProjectID'	: sponsorProject,
				'SponsorID'	: uid
			}
		});
	}


	if(advocateProject != "NULL") {
		$.ajax({
			"url"	: '../api/Project/editProject.php',
			'dataType' : 'json',
			'data' 	: {
				'ProjectID'	: advocateProject,
				'AdvocateID': uid
			}
		});
	}	

	// update project info based on sponosr
}


// Delete a profile 
function deleteProfile(uid) {
	bootbox.confirm("Are you sure you want to delete this user and all references to it?", function(confirmed){
		if(confirmed) {
			loadBig(true);
			$.ajax({
				'url'		: '../api/User/deleteUser.php',
				'dataType'	: 'json',
				'data'		: {
					'UserID'	: uid
				},
				'success'	: function(data) {
					loadBig(false);
					if(!data.Error) {
						bootbox.alert('Profile deleted successfully');
						Navigate('User-List');
					}
					else {
						bootbox.alert('Error: '+data.Error);
					}
				}
			})
		}
	});
}


// Create a new project with the new project button
$('#createUser').click(function(){
	bootbox.createUser("Create a user", function(data) {
		if(data) {
			if(data && data.Name && data.Name.length >1 && data.ClassID) {
				if(data.ClassID != "none") {
					loadBig(true);
					$.ajax({
						"url"		: "../api/User/createUser.php",
						"dataType"	: "json",
						"data"		: {
							"UserName" : data.Name,
							"ClassID"	  : data.ClassID,
							"Password"	: "cgt",
							"UserType"	: data.UserType
						},
						"success"	: function(d) {
							loadBig(false);
							if(!d.Error) {
								Navigate('User-List');
							}
							else {
								bootbox.alert("Error: "+d.Error);
							}
						}
					});
				}
				else {
					bootbox.alert("Error: To create a user, you must specify the class term");
				}
			}
			else {
				bootbox.alert('Error: Name must be longer than 1 character');
			}
		}
	})
});
