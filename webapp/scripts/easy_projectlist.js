// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.



$('#projectListClassFilter').change(DisplayProjectList);


function PopulateProjectListOptions() {

	// First let's populate the dropdowns
	$('#projectListClassFilter').html('');

	// Populate the 'all' options first
	$('#projectListClassFilter')
		.append( $('<option/>').attr('value','all').text('- All terms -') );


	// Populate classes from class list
	for( var i = 0; i < Classes.length; i++ ) {
		$('#projectListClassFilter')
			.append( $('<option/>').attr('value',Classes[i].ClassID).text(Classes[i].ClassTerm + " " + Classes[i].ClassYear) );
	}	0

}


function DisplayProjectList() {

	// Get the params from the dropdown lists
	var ClassID 		= $('#projectListClassFilter').val();

	var data = {};

	// Add the params needed to the data object
	if( ClassID != "all" ) 			data.ClassID = ClassID;

	// Make the ajax call
	loadBig(true);
	$.ajax({
		"url"		: "../api/Project/getProjects.php",
		"dataType"	: "json",
		"data"		: data,
		"success"	: function(data) {
			loadBig(false);

			// Destroy the old datatable
			$('#project_datatable').dataTable().fnDestroy();

			// Wipe the contents
			$('#projectlist-body').html('');

			// Now add the new data back in
			for(var i = 0; i < data.length; i++ ) {

				(function(project) {

					var uclass = {"ClassTerm" : "n/a", "ClassYear" : ""};

					// find the class term info
					for(var c = 0; c < Classes.length; c++ ) {
						if( Classes[c].ClassID == project.ClassID) {
							uclass = Classes[c];
						}
					}
					
					// Append the view button
					var actionCell = $('<td/>')
						.append($('<button/>')
							.addClass('btn btn-success')
							.text('View')
							.click(function(){
								viewProject( project.ProjectID );
							}));


					
					// Append the edit button if you have edit rights
					if( UserData.UserType == 3 || (project.SponsorID && project.SponsorID == UserData.UserID) || (project.AdvocateID && project.AdvocateID == UserData.UserID) ) {
						actionCell
							.append($('<button/>')
								.addClass('btn btn-info')
								.text('Edit')
								.click(function(){
									showEditProject(project.ProjectID);
								}));
					}

					// Append the delete button if you have admin rights
					if(  UserData.UserType == 3 ) {
						actionCell
							.append($('<button/>')
								.addClass('btn btn-danger')
								.text('Delete')
								.unbind('click')
								.click(function(){
									deleteProject(project.ProjectID);
								}));
					}

					

					// append a new row with all the data
					var projectRow = $('<tr/>')
						.append($('<td><a href="#">' + project.ProjectName + '</a></td>')
							.click(function(){viewProject(project.ProjectID)}))
						.append('<td class="projectSponsor">None</td>')
						.append('<td class="projectAdvocate">None</td>')
						.append('<td>' + uclass.ClassTerm + ' ' + uclass.ClassYear + '</td>')
						.append(actionCell)
						.appendTo( $('#projectlist-body'));


					// Look up the usernames of the sponsor and the advocate
					if(project.SponsorID) {
						projectRow.find('.projectSponsor').html('<img src="images/spinner-small.gif" alt="loading" />');
						getUser(project.SponsorID, function(user) {
							projectRow.find('.projectSponsor')
								.html('<a href="#">'+user.UserName+'</a>')
								.click(function(){viewProfile(user.UserID);});
						});
					}

					if(project.AdvocateID) {
						projectRow.find('.projectAdvocate').html('<img src="images/spinner-small.gif" alt="loading" />');
						getUser(project.AdvocateID, function(user) {
							projectRow.find('.projectAdvocate')
								.html('<a href="#">'+user.UserName+'</a>')
								.click(function(){viewProfile(user.UserID);});
						});
					}

				})(data[i]);			 	
			} 

			// Now we can build the users table
			buildProjectTable();

		}
	});

}



// This is called when you navigate to the userlist page
function ProjectListNavigate() {
	PopulateProjectListOptions();
	DisplayProjectList();
}

// Function to re-initialize the table
function buildProjectTable() {
	// Initialize again with new data
	$('#project_datatable').dataTable( {
		"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
		"sPaginationType": "bootstrap",
		"oLanguage": {
			"sLengthMenu": "_MENU_ projects per page"
		}
	});	
}