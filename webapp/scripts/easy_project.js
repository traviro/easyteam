// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.

function viewProject(pid) {

	loadBig(true);

	// Get the project information from the server
	getProject(pid, function(project) {
		loadBig(false);



		// Look up the usernames of the sponsor and the advocate
		if(project.SponsorID) {
			$('#projectSponsor').html('<img src="images/spinner-small.gif" alt="loading" />');
			getUser(project.SponsorID, function(user) {
				$('#projectSponsor')
					.html('<a href="#">'+user.UserName+'</a>')
					.unbind("click")
					.click(function(){viewProfile(user.UserID);});
			});
		}

		if(project.AdvocateID) {
			$('#projectAdvocate').html('<img src="images/spinner-small.gif" alt="loading" />');
			getUser(project.AdvocateID, function(user) {
				$('#projectAdvocate')
					.html('<a href="#">'+user.UserName+'</a>')
					.unbind("click")
					.click(function(){viewProfile(user.UserID);});
			});
		}

		var uclass;
		// find the class term info
		for(var c = 0; c < Classes.length; c++ ) {
			if( Classes[c].ClassID == project.ClassID) {
				uclass = Classes[c];
			}
		}		


		// Set  the correct information in the dom
		$('#projectName').text( project.ProjectName );

		$('#projectDescription').text( project.Description );	

		if(uclass) {
			$('#projectTerm').text( uclass.ClassTerm + ' ' + uclass.ClassYear );
		}	



		// Get your project rating info
		// set a loading bar on the stuff we are retreiving
		$('#rankingCount, #averageRanking, #medianRanking').html('<img src="images/spinner-small.gif" alt="loading" />');

		$.ajax({
				'url'		: '../api/ProjectRating/getProjectRatings.php',
				'dataType'	: 'json',
				'data'		: {
					'ProjectID'	: pid
				},
				'success'	: function(data) {
					$('#rankingCount, #averageRanking').html('');

					if(!data.Error) {

						// Loop through all of these project's rankings to find the average
						for( var i = 0; i < data.length; i++ ) {

							if(LoggedIn && data[i].UserID == UserData.UserID) {
								$('#projectRating').val( data[i].RatingVal );
							}

						}


						// Draw the chart visualization
						displayProjectRatingChart(data);
					}
					else {
						//  set as nothing if there are no rankings found
						$('#averageRanking').text('n/a');
						$('#rankingCount').text(0);
						$('#projectRating').val( 'none' );
					}
				}
			});
		// End project rating info section

	
		// Bind the click event for this project rating
		$('#submitRating').unbind('click').click(function() {
			var rating = $('#projectRating').val();
			loadBig(true);
			// Send the rating to the server
			$.ajax({
				'url'		: '../api/ProjectRating/createProjectRating.php',
				'dataType'	: 'json',
				'data'		: {
					'ProjectID'	: project.ProjectID,
					'RatingVal'	: rating
				},
				'success' 	: function(data) {
					loadBig(false);

					if(!data.Error) {

						viewProject(pid);
					}
					else {
						bootbox.alert(data.Error);
					}
				}
			})
		});
		// end bind rating button


		// Get project application status for this project
		$('#applicantNumber, #myApplication').html('<img src="images/spinner-small.gif" alt="loading" />');
		// Look up the applicants for this project
		$('#projectApplicants').html('<tr><td><img src="images/spinner-small.gif" alt="loading" /></td><td></td><td></td></tr>');		
		$.ajax({
			'url'		: '../api/ProjectApplication/getProjectApplications.php',
			'dataType'	: 'json',
			'data'		: {
				'ProjectID'	: project.ProjectID
			},
			'success'	: function(data) {
				//
				var hasApplication = false;
				var myApplication = {};
				$('#projectApplicants').html('');

				if( !data.Error ) {
					$('#applicantNumber').text(data.length);

					// See if the user has an application

					for(var i = 0; i < data.length; i++ ) {
						(function(app){
							if( app.UserID == UserData.UserID ) {
								hasApplication = true;
								myApplication = app;
							}
							// populate the applicants table
							var tablerow = $('<tr/>');
							$('#projectApplicants').append(tablerow);


							// add the buttons to add or drop a user based on an applicaiton
							var rembtn = $('<button class="btn btn-success" title="Remove this application"><i class="icon-off"></i></button>');
							var addbtn = $('<button class="btn" title="Accept this application"><i class="icon-off"></i></button>');
							var statustd = $('<td/>');


							if( app.ApplicationStatus == 0 || app.ApplicationStatus == "0" ) statustd.append(addbtn);
							else statustd.append(rembtn);


							// Allow administrative functions if  you're accepted
							if(UserData.UserType==3) {

								rembtn.click( function() {
									loadBig(true);
									$.ajax({
										'url'		: '../api/ProjectApplication/setProjectApplicationStatus.php',
										'dataType'	: 'json',
										'data'		: {
											'ProjectApplicationID' : app.ProjectApplicationID,
											'ApplicationStatus'	   : 0
										}
									});
									$.ajax({
										'url'		: '../api/User/editUser.php',
										'data'		: { ProjectID : 'NULL', UserID : app.UserID },  
										'success'	: function() {
											loadBig(false);
											viewProject(project.ProjectID);											
										}
									});
								});

								addbtn.click( function() {
									loadBig(true);
									$.ajax({
										'url'		: '../api/ProjectApplication/setProjectApplicationStatus.php',
										'dataType'	: 'json',
										'data'		: {
											'ProjectApplicationID' : app.ProjectApplicationID,
											'ApplicationStatus'	   : 1
										}
									});
									$.ajax({
										'url'		: '../api/User/editUser.php',
										'data'		: { ProjectID : project.ProjectID, UserID : app.UserID },
										'success'	: function() {
											loadBig(false);
											viewProject(project.ProjectID);											
										}
									});									
								});
							}


							// append all the info into the tablerow
							tablerow
								.append('<td>'+app.RatingVal+'</td>')
								.append($('<td><a href="#">'+app.UserName+'</a></td>')
									.click(function(){
										viewProfile(app.UserID);
									}))
								.append('<td><a href="'+app.ApplicationPath+'" target="_blank"><img src="images/pdf.png" alt="view app" title="view application" width="27" height="24"/></a></td>')
								.append(statustd);
						})(data[i]);								
					}

	
				}
				else {
					$('#applicantNumber').text(0);
				}

				// If the user has an application, let's append the appropriate buttons and functions
				if( hasApplication ) {
					$('#myApplication').html('')
						.append('Status: ' + AppStatuses[myApplication.ApplicationStatus] +'<br/><br/>' )
						.append($('<button class="btn btn-primary" title="view my application">View</button>')  // View application button
							.click(function(){
								viewApplication( myApplication );
							}))
						.append($('<button style="margin-left:20px;" class="btn btn-danger" title="delete application">Delete</button>')  // delete application button
							.click(function(){
								deleteApplication( myApplication.ProjectApplicationID, pid );
							}))
				}
				else {
					$('#myApplication').html('')
						.append($('<iframe src="uploadApplication.php?ProjectID=' + pid + '" seamless border="0" width="100%" height="80"></iframe>'));  // create application button

				}

			}
		});


		// Look up the members of this project
		$('#projectMembers').html('<tr><td><img src="images/spinner-small.gif" alt="loading" /></td></tr>');
		$.ajax({
			'url'		: '../api/User/getUsers.php',
			'dataType'	: 'json',
			'data'		: {
				'ProjectID'	: project.ProjectID,
				'Basic'		: 'true'
			},
			'success'	: function(data) {
				$('#projectMembers').html('');
				if(!data.Error) {
					if(data.length > 0 ) {
						for( var i = 0; i < data.length; i++ ) {
							(function(user) {
								$('#projectMembers').append($('<tr><td><a href="#">' + user.UserName + '</a></td></tr>')
									.click(function() {
										viewProfile(user.UserID);
									}));
							})(data[i]);
						}
					}
					else {
						$('#projectMembers').append('<tr><td>No Members</td></tr>');
					}
				}
			}
		});



	});


	Navigate('View-Project');
}




function deleteProject(pid) {
	
	// Confirm that you really want to delete this
	bootbox.confirm("Are you sure you would like to delete this project and all references to it?", function(confirmed) {
		if(confirmed) {
			loadBig(true);
			$.ajax({
				'url'		: '../api/Project/deleteProject.php',
				'dataType'	: 'json',
				'data'		: {
					'ProjectID'	: pid
				},
				'success'	: function(data) {
					loadBig(false);
					if(!data.Error) {
						bootbox.alert('Project deleted successfully');
						Navigate('Project-List');
					}
					else {
						bootbox.alert('Error: ' + data.Error);
					}
				}
			})

		}
	});
}










// =============== ProjectApplication functions =========================
// Function to delete the specified application
function deleteApplication(appid, pid) {
	bootbox.confirm("Are you sure you would like to delete this application?", function(confirmed) {
		if(confirmed) {
			loadBig(true);
			$.ajax({
				'url'		: '../api/ProjectApplication/deleteProjectApplication.php',
				'dataType'	: 'json',
				'data'		: {
					ProjectApplicationID	: appid 
				},
				'success' 	: function(data) {
					loadBig(false);
					if(!data.Error) {
						bootbox.alert('Application deleted successfully');

						if(pid) {
							viewProject(pid);
						}
					}
					else {
						bootbox.alert('Error: ' + data.Error);
					}
				}
			})


		}
	})
}


function viewApplication(app) {

	// let's just open a new window that the application path is pointing to
	window.open(app.ApplicationPath, '_blank');
}


// Create an application
// Called from the upload page that is put inside an iframe
function createApplication(path, pid) {

	loadBig(true);
	$.ajax({
		'url'		: '../api/ProjectApplication/createProjectApplication.php',
		'dataType'	: 'json',
		'data'		: {
			'ProjectID'	: pid,
			'UserID'	: UserData.UserID,
			'ApplicationPath'	: path
		},
		'success'	: function(data) {
			loadBig(false);
			if( !data.Error ) {
				viewProject(pid);
			}
			else {
				bootbox.alert('Error: '+data.Error);
			}
		}
	})

}



// Display the ratings chart with the current data
function displayProjectRatingChart(ratings) {


	var data = [0,0,0,0,0,0,0,0,0,0];

	var average = 0;
	for(var i = 0; i < ratings.length; i++) {
		data[ratings[i].RatingVal-1]++;
		average += ratings[i].RatingVal*1;
	}

	var max = -1;
	var median = 1;
	for( var i = 0; i < data.length; i++) {
		if(data[i] > max ) {
			max = data[i];
			median = i+1;
		}
	}


	if(ratings.length > 0)
		average = average/ratings.length;

	// set the average on the page
	$('#averageRanking').text(average);
	$('#rankingCount').text(ratings.length);
	$('#medianRanking').text(median);


	$('#ratingChart').highcharts({
        chart: {
            type: 'column',
            height: 120,
            margin: 0,
            marginBottom: 20
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: [
                '1',
                '2',
                '3',
                '4',
                '5',
                '6',
                '7',
                '8',
                '9',
                '10'
            ]
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        legend: {
        	enabled : false
        },
        credits: {
        	enabled: false
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">Rankings of {point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0"></td>' +
                '<td style="padding:0"><b>{point.y:.0f} Users</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Ratings 1-10',
            data: data

        }]
    });
}


// Function called to show the edit project for the specified edit project id
function showEditProject(pid) {
	$('.page').hide(100);
	$('#Edit-Project').show(100);
	$('#secondaryCrumb').text('Edit Profile');

	populateEditProject(pid)
}


// Populate the edit profile fields with the proper information about the editing target
function populateEditProject(pid) {
	loadBig(true);
	getProject(pid, function(project) {
		
		getUsers(function(users) {
			loadBig(false);

			// Set the dropdown options
			$('#sponsorInput, #pClassTermInput, #advocateInput').html('')
				.append('<option value="NULL">None</option>');
			for(var i = 0; i < Classes.length; i++) {
				$('<option/>')
					.attr('value', Classes[i].ClassID)
					.text(Classes[i].ClassTerm + ' ' + Classes[i].ClassYear)
					.appendTo($('#pClassTermInput'));
			}

			for(var i = 0; i < users.length; i++) {
				$('<option/>')
					.attr('value', users[i].UserID)
					.text(users[i].FirstName + ' ' + users[i].LastName)
					.appendTo($('#sponsorInput'));

				$('<option/>')
					.attr('value', Users[i].UserID)
					.text(users[i].FirstName + ' ' + users[i].LastName)
					.appendTo($('#advocateInput'));				
			}


			// Set the values
			$('.projectName').text(project.ProjectName);
			$('#projectNameInput').val(project.ProjectName);

			$('#sponsorInput').val(project.SponsorID);
			$('#advocateInput').val(project.AdvocateID);

			$('#descriptionInput').val(project.Description);
			$('#pClassTermInput').val(project.ClassID);


			// Bind the submit button to the right function call
			$('#submitEditProject').unbind('click')
				.click(function() {
					submitEditProject(pid);
				});
		});

	});
}


// Function call to the server to update a project
function submitEditProject(pid) {


	// Get the values
	var ProjectName = 	$('#projectNameInput').val();

	var SponsorID 	= 	$('#sponsorInput').val();
	var AdvocateID	= 	$('#advocateInput').val();

	var Description = $('#descriptionInput').val();
	var ClassID 	= $('#pClassTermInput').val();

	// Make the function call
	loadBig(true);
	$.ajax({
		'url'	: '../api/Project/editProject.php',
		'dataType'	: 'json',
		'data'	: {
			'ProjectID' 	: pid,
			'SponsorID'		: SponsorID,
			'AdvocateID'	: AdvocateID,
			'Description'	: Description,
			'ClassID'		: ClassID
		},
		'success' : function(data) {
			loadBig(false);
			if(!data.Error) {
				viewProject(pid);

			}
			else {
				bootbox.alert("Error: " + data.Error);
			}
		}
	})


}

// Create a new project with the new project button
$('#createProject').click(function(){
	bootbox.create("Create a project", function(data) {
		if(data) {
			if(data && data.Name && data.Name.length >1 && data.ClassID) {
				if(data.ClassID != "none") {
					loadBig(true);
					$.ajax({
						"url"		: "../api/Project/createProject.php",
						"dataType"	: "json",
						"data"		: {
							"ProjectName" : data.Name,
							"ClassID"	  : data.ClassID
						},
						"success"	: function(d) {
							loadBig(false);
							if(!d.Error) {
								Navigate('Project-List');
							}
							else {
								bootbox.alert("Error: "+d.Error);
							}
						}
					});
				}
				else {
					bootbox.alert("Error: To create a project, you must specify the class term");
				}
			}
			else {
				bootbox.alert('Error: Name must be longer than 1 character');
			}			
		}
	})
});


