// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.


var groupOddInterval = "";

var projectList = {};

function displayGroupOdds() {

	$('#groupOddsClass').html('');
	$('#groupOddsClass').append('<option value="all">All classes</option>');	
	
	for(var i = 0; i < Classes.length; i++) {
		$('#groupOddsClass')
			.append('<option value="' + Classes[i].ClassID + '">' + Classes[i].ClassTerm + ' ' + Classes[i].ClassYear + '</option>');
	}

	getProjectForOdds();

	groupOddInterval = setInterval(drawGroupOddsScatter, 30000);

	// refresh project list when we change the dropdown
	$('#groupOddsClass').unbind("change").change(getProjectForOdds);	

}


// get this list of projects so we can get the group odds based on them
function getProjectForOdds() {
	projectList = {};
	loadBig(true);
	var data = {};
	if($('#groupOddsClass').val() != 'all') data.ClassID = $('#groupOddsClass').val();

	$.ajax({
		'url'		: '../api/Project/getProjects.php',
		'dataType'	: 'json',
		'data'		: data,
		'success'	: function(d) {
			loadBig(false);
			if(!d.Error) {

				for(var i = 0; i < d.length; i++) projectList[ d[i].ProjectID ] = {rawmean: 0, total: 0, name: d[i].ProjectName};
				drawGroupOddsScatter();
			}
		}
	});
}



// get the ratings, then draw the graph
function drawGroupOddsScatter() {

	loadSmall(true);
	$.ajax({
		'url'		: '../api/ProjectRating/getProjectRatings.php',
		'dataType'	: 'json',
		'success'	: function(data) {
			loadSmall(false);
			if(!data.Error) {

				for(var i in projectList ) { projectList[i].total = 0; projectList[i].rawmean = 0; }


				// filter the data based on project
				for(var i = 0; i < data.length; i++ ) {

					if( projectList[ data[i].ProjectID ] ) {
						projectList[ data[i].ProjectID ].total++;
						projectList[ data[i].ProjectID ].rawmean+= data[i].RatingVal*1;
					}

				}

				var mySeries = [];

				for(var i in projectList ) {

					var avg = 0;
					if( projectList[i].total > 0 ) avg = projectList[i].rawmean/projectList[i].total;

					mySeries.push({
						"name"	: projectList[i].name,
						"data"	: [[ projectList[i].total, avg  ]]
					});
				}



				// now draw the graph
				$('#groupOddsVis').highcharts({
		            chart: {
		                type: 'scatter',
		                zoomType: 'xy',
		                marginLeft: 250
		            },
		            title: {
		                text: 'Live Project Ratings Visualization'
		            },
		            credits: {
		            	enabled: false
		            },
		            xAxis: {
		                title: {
		                    enabled: true,
		                    text: 'Number of Ratings'
		                },
		                startOnTick: true,
		                endOnTick: true,
		                showLastLabel: true
		            },
		            yAxis: {
		                title: {
		                    text: 'Average Rating'
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'left',
		                verticalAlign: 'top',
		                x: 0,
		                y: 10,
		                floating: true,
		                backgroundColor: '#FFFFFF',
		                borderWidth: 1,
		                width: 170
		            },
		            plotOptions: {
		                scatter: {
		                    marker: {
		                        radius: 5,
		                        states: {
		                            hover: {
		                                enabled: true,
		                                lineColor: 'rgb(100,100,100)'
		                            }
		                        }
		                    },
		                    states: {
		                        hover: {
		                            marker: {
		                                enabled: false
		                            }
		                        }
		                    },
		                    tooltip: {
		                        headerFormat: '<b>{series.name}</b><br>',
		                        pointFormat: '{point.x} rating, {point.y} average'
		                    }
		                }
		            },
		            series: mySeries 
		        });



			}
		}
	});

	return;














}