// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.

function displayDashboard() {

	//get the main message from the server

	$('#mainMessageDisplay').html('<img src="images/spinner-small.gif" alt="loading" />');
	$.ajax({
		'url'		: '../api/MainMessage/getMainMessage.php',
		'dataType'	: 'json',
		'success'	: function(data) {
			loadBig(false);
			 if(!data.Error) {
			 	$('#mainMessageDisplay').html(data.MainContent);
			 }
			 else {
			 	$('#mainMessageDisplay').html('No message found');
			 }
		}
	});	


	// get the recent activity
	$('#recentDiv').html('<img src="images/spinner-small.gif" alt="loading" />');

	$.ajax({
		'url'		:  '../api/Message/getMessages.php',
		'dataType'	: 'json',
		'data'		: {
			MessageType : 'Global'
		},
		'success'	: function(data) {
			if(!data.Error || data.length > 0) {
				var length = 15;
				if(data.length < 15) length = data.length;


				for( var i = 0; i < data.length; i++) {
					$('#recentDiv').append('<h3>'+data[i].MessageTitle+'</h3>');
					$('#recentDiv').append('<p>'+data[i].MessageContent+'</p>');
				}


			}
			else {
				$('#recentDiv').html('No recent activity');
			}





		}
	});
}