// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.


// Display the inbox
function DisplayInbox() {

	loadSmall(true);

	// Get pid
	var pid;
	if(UserData.ProjectID) {
		pid = UserData.ProjectID;
	}
	else {
		pid = -1;
	}

	var status = ["Unread", "Read"];

	$.ajax({
		"url" 		: "../api/Message/getMessagesCombined.php",
		"dataType"	: "json",
		"data"		: {
				"UserID"	: UserData.UserID,
				"ProjectID"	: pid
			},
		"success"	: function(data) {
			loadSmall(false);

			// Destroy the old datatable
			$('#inbox_datatable').dataTable().fnDestroy();
			$('#inbox-body').html('');	

			if( !data.Error ) {


				// Go through and insert the messages into the datatable
				for( var i = 0; i < data.length; i++) {
					// Encapsulate message in a function
					(function(message) {
						var name = message.UserName + " ("+ message.MessageType+")";
						// Append each message row
						$('<tr/>')
							.append($('<td>' + message.MessageTitle + '</td>')
								.css('cursor','pointer')
								.click(function() {
									viewMessage( message );
								}))
							.append($('<td class="center"><a href="#">' + name + '</a></td>')
								.click(function(){
									viewProfile(message.UserID);
								}))
							.append('<td class="center">' + message.MessageDate + '</td>')
							.append('<td class="center">' + status[ message.MessageSeen ] + '</td>')
							.appendTo( $('#inbox-body') )
							.addClass('messageRow ' + status[ data[i].MessageSeen ])
							.append(
								$('<td/>')
									.css('text-align', 'center')
									.append(
										$('<button/>')
											.addClass("btn")
											.text("Delete")
											.click(function() {
												deleteMessage( message.MessageID );
											})
									)										
									.append(
										$('<button/>')
											.addClass("btn")
											.text("Mark as read")
											.click(function() {
												markAsRead( message.MessageID );
											})
									)									
									.append(
										$('<button/>')
											.addClass("btn btn-primary")
											.text("View")
											.click(function() {
												viewMessage( message );
											})
									)															
							);

					})(data[i]);

				}

				// Now initialize the data table
				buildMessageTable();
				
			}
			else {
				bootbox.alert("" + data.Error);
			}
		}
	})


	// Function to re-initialize the table
	function buildMessageTable() {
		// Initialize again with new data
		$('#inbox_datatable').dataTable( {
			"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"sPaginationType": "bootstrap",
			"oLanguage": {
				"sLengthMenu": "_MENU_ messages per page"
			}
		});	
	}
	
}


function viewMessage(message) {
    var htmlString =  "<h3>"+message.MessageTitle+"</h3><strong>From</strong><br/>" + message.UserName + "<br/><strong>Date</strong><br/>" + message.MessageDate + "<br/><br/><strong>Message</strong><br/>" + message.MessageContent;

    bootbox.dialog(htmlString,[
	    //buttons
	    {
	        "label" : "Reply",
	        "class" : "btn-primary pull-left",
	        "callback": function(e){

	        	$('#messageTitleInput').val('Re: ' + message.MessageTitle);
	            Navigate('Compose-Message');
	        }   
	    },	    
	    {
	        "label" : "Delete Message",
	        "class"	: "btn pull-left",
	        "callback": function(e){
	           deleteMessage( message.MessageID );
	        }
	    },
	    {
	        "label" : "Ok",
	        "class" : "btn-success",
	        "callback": function(e){
	            //do stuff
	        }   
	    }					    
	]); // -- end bootbox -- //
}


// Delete a message with the given ID
function deleteMessage(MessageID) {
	bootbox.confirm("Are you sure you want to delete this message?", function(response) {
		if(response) {
			loadSmall(true);
			$.ajax({
				"url"		: "../api/Message/deleteMessage.php",
				"dataType"	: "json",
				"data"		: {
					"MessageID"	: MessageID
				},
				"success"	: function(data) {
					loadSmall(false);
					if(data.Error) {
						bootbox.alert("Error: "+data.Error);
					}
					else {
						DisplayInbox();
						getTotalMessageCount(UserData.UserID);
					}
				}
			});
		}
	});
}

function markAsRead(MessageID) {
	loadSmall(true);
	$.ajax({
		"url"		: "../api/Message/markReadMessage.php",
		"dataType"	: "json",
		"data"		: {
			"MessageID"	: MessageID
		},
		"success"	: function(data) {
			loadSmall(false);
			if(data.Error) {
				bootbox.alert("Error: "+data.Error);
			}
			else {
				DisplayInbox();
			}
		}
	});
}


function getUnreadMessageCount(UserID) {
	loadSmall(true);
	$.ajax({
		"url"		: "../api/Message/getUnreadMessageCount.php",
		"dataType"	: "json",
		"data"		: {
			"UserID"	: UserID
		},
		"success"	: function(data) {
			loadSmall(false);
			if(data.Error) {
				bootbox.alert("Error: "+data.Error);
			}
			else {
				$('.messageCount').text( (data.MessageCount) );
			}
		}
	});
}

function getTotalMessageCount(UserID) {
	//loadSmall(true);
	$.ajax({
		"url"		: "../api/Message/getTotalMessageCount.php",
		"dataType"	: "json",
		"data"		: {
			"UserID"	: UserID
		},
		"success"	: function(data) {
			//loadSmall(false);
			if(data.Error) {
				bootbox.alert("Error: "+data.Error);
			}
			else {
				$('.totalMessageCount, .messageCount').text( (data.MessageCount) );
			}
		}
	});
}



function showSpecificRecipient(e) {

	var type = $(e.target).val();

	$('#messageRecipientInputDiv').hide(100);

	// show the additional dropdown
	if( type == "Project" || type == "User" ) {

		// first show loading spinner
		$('#mesSpecificLoading').show(100);



		if( type == "Project" ) {
			// Now make the ajax function call
			$.ajax({
				"url"		: "../api/Project/getProjects.php",
				"dataType"	: "json",
				"success"	: function(data) {

					$('#mesSpecificLoading').hide(100);

					if( !data.Error ) {
						// Show dropdown div
						$('#messageRecipientInputDiv').show(100);

						// clear out old contents
						$('#messageRecipientInput').html('');

						// set new contents
						for(var i = 0; i < data.length; i++) {
							$('<option/>')
								.attr('value', data[i].ProjectID )
								.text(data[i].ProjectName)
								.appendTo( $('#messageRecipientInput') );
						};

					}
					else {
						bootbox.alert("Error: " + data.Error);
					}

				}
			});
		}	
		else if( type == "User" ) {
			$.ajax({
				"url"		: "../api/User/getUsers.php",
				"dataType"	: "json",
				"success"	: function(data) {

					$('#mesSpecificLoading').hide(100);

					if( !data.Error ) {

						// Show dropdown div
						$('#messageRecipientInputDiv').show(100);

						// clear out old contents
						$('#messageRecipientInput').html('');


						// set new contents
						for(var i = 0; i < data.length; i++) {


							var nametext = data[i].UserName;
							if( data[i].FirstName && data[i].LastName ) nametext = data[i].FirstName + ' ' + data[i].LastName + ' (' + data[i].UserName + ')';							
							$('<option/>')
								.attr('value', data[i].UserID )
								.text(nametext)
								.appendTo( $('#messageRecipientInput') );
						};
					}					
					else {
						bootbox.alert("Error: " + data.Error);
					}

				}
			});
		}
	}

}


// send a message when the send message button was clicked
function sendMessage() {

	// Get the values from the html form
	var recipient 			= $('#messageRecipientTypeInput').val();
	var specificRecipient 	= $('#messageRecipientInput').val();
	var messageTitle 		= $('#messageTitleInput').val();
	var messageContent 		= $('#messageContentInput').val();
	var isTextMessage 		= $('#messageIsTextInput').get(0).checked;
	var messageType = "User";
	var toID = "";

	toID = specificRecipient;

	if( recipient == "User" ) messageType = "User";
	if( recipient == "Project" ) messageType = "Project";
	if( recipient == "Global" ) messageTpe = "Global";
	if( recipient == "My-Project" ) {
		messageTpe = "Project";
		toID = UserData.ProjectID;
	}

	


	// Execute the ajax request
	loadBig(true);

	$.ajax({
		"url"		: "../api/Message/createMessage.php",
		"dataType"	: "json",
		"data"	: {
				"MessageType"	: messageType,
				"UserID"		: UserData.UserID,
				"MessageContent": messageContent,
				"ToID"			: toID,
				"MessageTitle"	: messageTitle
			},
		"success"	: function(data) {
			loadBig(false);
			if(!data.Error) {

				bootbox.alert("Message delivered successfully");

			}
			else {
				bootbox.alert("Error: " + data.Error);
			}
		}
	});


	var sendAsText = $('#messageIsTextInput').get(0).checked;

	if(sendAsText) {


		
	}

}
