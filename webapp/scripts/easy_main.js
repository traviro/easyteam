// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.

var UserTypes = {
	1	: "Student",
	2	: "Sponsor",
	3 	: "Administrator"
};

var AppStatuses = {
	0	: "Pending Review",
	1 	: "Approved",
	2 	: "Denied"
};

var UserData = {};
var LoggedIn = false;
var AreasOfFocus = [];
var Classes = [];
var Users = [];
var ActiveClass = {};

var checkMessageInterval;



// functions to be called when navigating to certain pages
var NavFunctionMap = {
	"Calendar"	: DisplayCalendar,
	"Inbox"		: DisplayInbox,
	"Edit-Profile" : editMyProfile,
	"User-List"	: UserListNavigate,
	"Project-List" : ProjectListNavigate,
	"Class-Management" : showClassManagement,
	"Dashboard"		: displayDashboard,
	"Group-Odds"	: displayGroupOdds,
	"Link-List"		: DisplayLinkList
};



$(document).ready(function(){
	hideLoggedInContent();
	Navigate("Dashboard");


	// init datetimepickers
    $('.datetimepicker').datetimepicker({
      language: 'en',
      pick12HourFormat: true,
      maskInput: true
    });	

    // init tooltips
    $("[rel=tooltip]").tooltip({ placement: 'right'});

	// Get the class list
	getClasses();    

	// Get the areas of focus
	getAreasOfFocus(function() {});

    setInterval(function() {
		// Get the class list
		getClasses();    

		// Get the areas of focus
		getAreasOfFocus(function() {});
    }, 120000);



});


// Event hooks -----///
$('.navlink').click( function(e) {
	// Highlight the selected button
	$('.navlink').removeClass('active');
	$(this).addClass('active');

	clearInterval(groupOddInterval);

	Navigate( $(this).attr('rel') );

});

// Bind my project
$('#MyProject-Nav').unbind("click").click( function(e) {

	if( UserData.ProjectID ) {
		// Highlight the selected button
		$('.navlink').removeClass('active');
		$(this).addClass('active');

		viewProject( UserData.ProjectID);
	}
	else {
		bootbox.alert("You are not assigned to a project yet");
	}
});

$('.logoutButton').click(Logout);

$('.loginButton').click(Login);

$('#calendarItemTypeInput').change(function(e) { showSpecificCalendar(e); } );

$('#addCalendarEvent').click(addCalendarItem);

$('#submitSendMessage').click(sendMessage);



$('#messageRecipientTypeInput').change( showSpecificRecipient );


// Easyteam Functions
// These functions control various elements on the page, and are sometimes called from multiple DOM element hooks

function Navigate(id) {
	$('.page').hide();
	$('#'+id).fadeIn(400);
	$('#secondaryCrumb').text(id);

	// Call any function that may be mapped
	if(NavFunctionMap[id]) {
		(NavFunctionMap[id])();
	}
}

function Logout() {
	hideLoggedInContent();
	Navigate('Dashboard');
	$('.navlink').removeClass('active');
	$('#Dashboard-Nav').addClass('active');

	$.ajax({
		"url"	: "../api/Functions/logout.php"
	});
	LoggedIn = false;

	clearInterval(checkMessageInterval);
}

function Login() {
	bootbox.login("Log in to CGT 411", function(response) {
		if(response) {

			loadBig(true)

			$.ajax({
				"url" 	: "../api/Functions/login.php",
				"dataType" : "json",
				"data"  : {
						"UserName" : response.UserName,
						"Password" : response.Password
					},
				success : function(data) {

					loadBig(false);
					// If the the login was a success with correct username and password
					if(!data.Error) {

						// Set global userdata object as the data
						UserData = data;
						LoggedIn = true;

						// Set page content
						$('.username').text(data.UserName);
						$('.usertype').text(UserTypes[data.UserType]);
						$('.firstName').text(data.FirstName);


						// Show the logged in content
						showLoggedInContent();

						//getUnreadMessageCount(UserData.UserID);
						getTotalMessageCount(UserData.UserID);

						// check for new messages every 60 seconds
						checkMessageInterval = setInterval(function(){
							getTotalMessageCount(UserData.UserID);
						}, 60000);

						// Show admin options only if you are an admin
						if( data.UserType == 3 ) {
							showAdminContent();
						}

						// Hide group options if you are not part of a group yet
						if( !data.ProjectID )
							$('option[value="My-Project"]').css('display','none');

						// Navigate to the dashboard
						$('.navlink').removeClass('active');
						$('#Dashboard-Nav').addClass('active');						
						Navigate('Dashboard');

					}
					else {
						bootbox.alert("Error: " + data.Error);
					}
				},
				error	: function(a, status) {
					bootbox.alert("Error: " + status);
				}
				
			});
		}
	});
}

// Hide all logged-in specific content
function hideLoggedInContent() {
	$('.loginOnly, .adminOnly').hide(300);
	$('.logoutOnly').show(300);

}

// show all logged in content
function showLoggedInContent() {
	$('.loginOnly').show(300);
	$('.logoutOnly').hide(300);
}


function showAdminContent() {
	$('.adminOnly').show(300);
}

function hideAdminContent() {
	$('.adminOnly').show(300);
}

function loadBig(on) {
	if(on) 
		$('#loading-overlay').css('display', 'block');
	else
		$('#loading-overlay').css('display', 'none');	
}

function loadSmall(on) {
	if(on) 
		$('#soft-load').show(100);
	else
		$('#soft-load').hide(100);	
}



// Set global area of focus array
// Call callback function
function getAreasOfFocus(cb) {
	$.ajax({
		"url" 		: "../api/AreaOfFocus/getAreasOfFocus.php",
		"dataType" 	: "json",
		"success"	: function(data) {
			AreasOfFocus = data;
			cb();
		}
	});
}

// Get object containing all the users and store it for later use in the program
function getUsers(cb) {
	$.ajax({
		"url" 		: "../api/User/getUsers.php",
		"dataType" 	: "json",
		"data"		: {
				"Basic" : "true"
			},
		"success"	: function(data) {
			if( !data.Error ) {
				Users = data;
				cb(Users);
			}
			else {
				bootbox.alert("Error: " + data.Error);
			}
		}
	});
}

// function to get a user's info given their id
function getUser(id, cb) {
	// Null case
	$.ajax({
		"url" 		: "../api/User/getUsers.php",
		"dataType" 	: "json",
		"data"		: {
				"UserID" : id
			},
		"success"	: function(data) {
			if( !data.Error ) {
				if( data.length == 1 ) {
					cb( data[0] );
				}
				else {
					cb({'UserName':'None'});
				}
			}
			else {
				bootbox.alert("Error: " + data.Error);
			}
		}
	});
}


// function to get a project's info given their id
function getProject(id, cb) {
	$.ajax({
		"url" 		: "../api/Project/getProjects.php",
		"dataType" 	: "json",
		"data"		: {
				"ProjectID" : id
			},
		"success"	: function(data) {
			if( !data.Error ) {
				if( data.length == 1 ) {
					cb( data[0] );
				}
				else {
					bootbox.alert("Error: Project not found");
				}
			}
			else {
				bootbox.alert("Error: " + data.Error);
			}
		}
	});
}

// Set  the class list and the active class
function getClasses() {
	$.ajax({
		"url"		: "../api/Class/getClasses.php",
		"dataType"	: "json",
		"success"	: function(data) {
			if( !data.Error ) {
				Classes = data;

				for(var i = 0; i < data.length; i++) {
					if(data[i].ClassStatus == 1 ) {
						ActiveClass = data[i];
					}
				}
			}
		}
	})
}






// return a correctly formatted timestamp for mysql given the inputted time string
function formatTimestamp(d) {
	var stamp = d.getFullYear() + "-" + pad(d.getMonth()+1) + "-" + pad(d.getDate()) + " " + pad(d.getHours()) + ":" + pad(d.getMinutes()) + ":" + pad(d.getSeconds());
	return stamp;
}

function pad(num) {
	var str = num + "";
	if( str.length == 1 ) return "0" + str;
	else return str;
}