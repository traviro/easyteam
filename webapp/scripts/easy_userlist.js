// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.



$('#userListClassFilter, #userListAOFFilter, #userListAcountFilter').change(DisplayUserList);


function PopulateUserListOptions() {

	// First let's populate the dropdowns
	$('#userListClassFilter, #userListAOFFilter').html('');

	// Populate the 'all' options first
	$('#userListClassFilter')
		.append( $('<option/>').attr('value','all').text('- All terms -') );

	$('#userListAOFFilter')
		.append( $('<option/>').attr('value','all').text('- All areas of focus -') );	


	// Populate classes from class list
	for( var i = 0; i < Classes.length; i++ ) {
		$('#userListClassFilter')
			.append( $('<option/>').attr('value',Classes[i].ClassID).text(Classes[i].ClassTerm + " " + Classes[i].ClassYear) );
	}	

	// Populate areas of focus
	for( var i = 0; i < AreasOfFocus.length; i++ ) {
		$('#userListAOFFilter')
			.append( $('<option/>').attr('value',AreasOfFocus[i].AreaOfFocusID).text(AreasOfFocus[i].AreaOfFocusName) );
	}

}


function DisplayUserList() {

	// Get the params from the dropdown lists
	var ClassID 		= $('#userListClassFilter').val();
	var AreaOfFocusID 	= $('#userListAOFFilter').val();
	var UserType		= $('#userListAcountFilter').val();

	var data = {};

	// Add the params needed to the data object
	if( ClassID != "all" ) 			data.ClassID = ClassID;
	if( AreaOfFocusID != "all" ) 	data.AreaOfFocusID = AreaOfFocusID;
	if( UserType != "all" )			data.UserType = UserType;

	// Make the ajax call
	loadBig(true);
	$.ajax({
		"url"		: "../api/User/getUsers.php",
		"dataType"	: "json",
		"data"		: data,
		"success"	: function(data) {
			loadBig(false);

			// Destroy the old datatable
			$('#users_datatable').dataTable().fnDestroy();

			// Wipe the contents
			$('#userlist-body').html('');

			// Now add the new data back in
			for(var i = 0; i < data.length; i++ ) {

				(function(user) {


					var uclass = {"ClassTerm" : "n/a", "ClassYear" : ""};

					// find the class term info
					for(var c = 0; c < Classes.length; c++ ) {
						if( Classes[c].ClassID == user.ClassID) {
							uclass = Classes[c];
						}
					}


					// Append the view button
					var actionCell = $('<td/>')
						.append($('<button/>')
							.addClass('btn btn-success')
							.text('View Profile')
							.click(function() {
								viewProfile(user.UserID);
							}));


					// Append the edit button if you have edit rights
					if( UserData.UserType == 3 || user.UserID == UserData.UserID) {
						actionCell
							.append($('<button/>')
								.addClass('btn btn-info')
								.text('Edit')
								.click(function(){
									showEditProfile(user.UserID);
								}));
					}

					// Append the delete button if you have admin rights
					if(  UserData.UserType == 3 ) {
						actionCell
							.append($('<button/>')
								.addClass('btn btn-danger')
								.text('Delete')
								.click(function(){
									deleteProfile(user.UserID);
								}));
					}

					var nametext = user.UserName;
					if( user.FirstName && user.LastName ) nametext = user.FirstName + ' ' + user.LastName + ' (' + user.UserName + ')';

					var porttext = '<td><a target="_blank" href="'+user.PortfolioURL+'">'+user.PortfolioURL+'</a></td>';
					if( !user.PortfolioURL ) porttext = '<td>N/A</td>';


					// append a new row with all the data
					var userRow = $('<tr/>')
						.append($('<td><a href="#">'+nametext+'</a></td>')
							.click(function(){ viewProfile(user.UserID); }))
						.append(porttext)
						.append('<td class="project"></td>')
						.append('<td>' + uclass.ClassTerm + ' ' + uclass.ClassYear + '</td>')
						.append(actionCell)
						.appendTo( $('#userlist-body'));


					if(user.ProjectID) {
						//console.log(user.ProjectID);
						getProject(user.ProjectID, function(project) {
							userRow.find('.project')
								.html('<a href="#">'+project.ProjectName+'</a>')
								.click(function(){ viewProject(project.ProjectID) });

						});
					}

				})(data[i]);
			} 

			// Now we can build the users table
			buildUserTable();

		}
	});

}



// This is called when you navigate to the userlist page
function UserListNavigate() {
	PopulateUserListOptions();
	DisplayUserList();
}

// Function to re-initialize the table
function buildUserTable() {
	// Initialize again with new data
	$('#users_datatable').dataTable( {
		"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
		"sPaginationType": "bootstrap",
		"oLanguage": {
			"sLengthMenu": "_MENU_ users per page"
		}
	});	
}