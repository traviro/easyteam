// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.

$('#primaryCategory').change(DisplayLinkList);


function DisplayLinkList() {


	var data = {};
	// Get the params from the dropdown lists
	var PrimaryCategory = $('#primaryCategory').val();

	if(PrimaryCategory != "all") data.PrimaryCategory = PrimaryCategory;


	// Make the ajax call
	loadBig(true);
	$.ajax({
		"url"		: "../api/Link/getLinks.php",
		"dataType"	: "json",
		"data"		: data,
		"success"	: function(data) {
			loadBig(false);

			// Destroy the old datatable
			$('#links_datatable').dataTable().fnDestroy();

			// Wipe the contents
			$('#linklist-body').html('');

			// Now add the new data back in
			for(var i = 0; i < data.length; i++ ) {

				(function(link) {



					var actionCell = $('<td/>');


					if(UserData.UserType == 3) {
						actionCell
							.append($('<button class="btn btn-danger">X</button>')
								.click(function(){
									$.ajax({
										'url' 	: '../api/Link/deleteLink.php',
										'data'  : {LinkID : link.LinkID},
										'success' : function() {
											DisplayLinkList();
										}
									});
								}));
					}



					// append a new row with all the data
					var userRow = $('<tr/>')
						.append($('<td><a target="_blank" href="'+link.URL+'">'+  link.LinkDescription +'</a></td>'))
						.append('<td>'+link.PrimaryCategory+'</td>')						
						.append(actionCell)
						.appendTo( $('#linklist-body'));


				})(data[i]);
			} 

			// Now we can build the users table
			buildLinkTable();

		}
	});

}




// Function to re-initialize the table
function buildLinkTable() {
	// Initialize again with new data
	$('#links_datatable').dataTable( {
		"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
		"sPaginationType": "bootstrap",
		"oLanguage": {
			"sLengthMenu": "_MENU_ links per page"
		}
	});	
}


$('#addLink').click(function() {
	bootbox.createLink("Add a link", function(d) {
		if(d) {
			//console.log(d);
			if(d.LinkDescription.length < 2) d.LinkDescription = "none";


			$.ajax({
				'url' 		: '../api/Link/createLink.php',
				'dataType'	: 'json',
				'data'		: d,
				'success'	: function(data) {
					if(!data.Error) {
						bootbox.alert("Link added successfully");
						DisplayLinkList();
					}
					else{
						bootbox.alert("Error: " + data.Error);
					}

				}
			})

		}



	});




});