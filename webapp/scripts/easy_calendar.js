// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.


// Display the calendar using events from the server
// Ridiculous multiple ajax calls because I don't feel like making a new php function to cover all the cases
function DisplayCalendar() {

	// The calendar events
	var events = [];

	loadBig(true);

	// First let's get the requisite calendar items from the database
	$.ajax({
		"url"		: "../api/CalendarItem/getCalendarItems.php",
		"dataType"	: "json",
		"data"		: {
				"CalendarItemType" : "Global"
			},
		"success"	: function(data) {
			// If there was no problem getting the cal items, store them
			if( !data.Error ) {
				// add the events into the array
				events = events.concat(data);
				// If logged in we want to get the user-specific cal events
				if(LoggedIn) {


					// First let's get the requisite calendar items from the database
					$.ajax({
						"url"		: "../api/CalendarItem/getCalendarItems.php",
						"dataType"	: "json",
						"data"		: {
								"CalendarItemType" : "User",
								"ToID" : UserData.UserID
							},
						"success"	: function(data) {
							// If there was no problem getting the cal items, store them
							if( !data.Error ) {
								// add the events into the array
								events = events.concat(data);
								// If in a project we want to get the project-specific cal events
								if(UserData.ProjectID) {


									// First let's get the requisite calendar items from the database
									$.ajax({
										"url"		: "../api/CalendarItem/getCalendarItems.php",
										"dataType"	: "json",
										"data"		: {
												"CalendarItemType" : "Project",
												"ToID" : UserData.ProjectID
											},
										"success"	: function(data) {
											// If there was no problem getting the cal items, store them
											if( !data.Error ) {
												// add the events into the array
												events = events.concat(data);
												
												cal(events);
											}
											else {
												bootbox.alert("Error: " + data.Error);
												cal(events);
											}
										}
									});  // -- end ajax call () -- //


								}
								else {
									cal(events);
								}
							}
							else {
								bootbox.alert("Error: " + data.Error);
								cal(events);
							}
						}
					});  // -- end ajax call () -- //

				}
				else {
					cal(events);
				}

			}
			else {
				bootbox.alert("Error: " + data.Error);
				cal(events);
			}
		}
	}); // -- end ajax call () -- //




	// sub-function that actually ddoes the displaying
	function cal(evts) {

		loadBig(false);
		$('#calendar').html('');

		var eventColors = {
			"Global"	: "#3366cc",
			"User"		: "#b833cc",
			"Project"	: "#33cc66"
		}

		// poarse the events
		var calEvents = [];

		for(var i = 0; i < evts.length; i++ ) {
			calEvents.push({
				title	: evts[i].CalendarItemTitle,
				start	: evts[i].DateStart,
				end		: evts[i].DateEnd,
				id 		: evts[i].CalendarItemID*1,
				backgroundColor : eventColors[evts[i].CalendarItemType],
				allDay : false

			});
		};	

		// Calendar
		var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();

		$('#calendar').fullCalendar({
			theme: false,
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay '
			},

			aspectRatio: 2,
			editable: false,
			events: calEvents,
		    eventClick: function(calEvent, jsEvent, view) {

		        // Lookup the id of this calEvent
				var desc = "";
		        for(var i = 0; i < evts.length; i++) {
		        	if( calEvent.id = evts[i].CalendarItemID ) {
		        		desc = evts[i].CalendarItemContent;
		        	}
		        }

		        var htmlString =  "<h3>"+calEvent.title+"</h3><strong>Description</strong><br/>" + desc + "<br/><br/><strong>Start Time</strong><br/>" + calEvent.start + "<br/><br/><strong>End Time</strong><br/>" + calEvent.end;

		        bootbox.dialog(htmlString,[
				    //buttons
				    {
				        "label" : "Delete Event",
				        "class"	: "btn pull-left",
				        "callback": function(e){
				           deleteCalendarItem( calEvent.id );
				        }
				    },
				    {
				        "label" : "Ok",
				        "class" : "btn-success",
				        "callback": function(e){
				            //do stuff
				        }   
				    }				    
				]); // -- end bootbox -- //


		    }			
		});	

	}
	
} // - End displayCalendar function ----//


// Delete a calendar Item
function deleteCalendarItem(id) {
	// throw up a confirm box to be certain first
	bootbox.confirm("Are you sure you want to delete this calendar item?", function(result) {
		if( result ) {
			loadBig(true)
			$.ajax({
				"url"	: "../api/CalendarItem/deleteCalendarItem.php",
				"dataType" : "json",
				"data" 	: {
						"CalendarItemID" : id
					},
				"success" : function(data) {

					loadBig(false);

					if( !data.Error ) {
						// successful delete
						DisplayCalendar();
					}
					else {
						// There was an error
						bootbox.alert("Error: " + data.Error);
					}
					
				}
			});
		}
	});
}


// Add a calendar Item
// This function is called when the user clicks the button to add a calendar item
function addCalendarItem() {

	//console.log("clicked");

	// Pull the values from the forms
	var title = $('#calendarItemTitleInput').val();
	var desc = $('#calendarItemContentInput').val();
	var target = $('#calendarItemTypeInput').val();
	var specifically = $('#calendarSpecificInput').val();
	var startDate = $('#startDateInput').val();
	var endDate = $('#endDateinput').val();


	// determine the values that we need
	try {
		var start = new Date(startDate);

		try {
			var end = new Date(endDate);

			if( end.getTime() > start.getTime() ) {

				var toID;
				var calendarItemType;

				if( target == "Me") {
					toID = UserData.UserID;
					calendarItemType = "User";
				}
				else if ( target == "My-Project") {
					toID = UserData.ProjectID;
					calendarItemType = "Project";
				}
				else if( target == "Project" ){
					toID = specifically;
					calendarItemType = "Project";
				}
				else if ( target == "User" ) {
					toID = specifically;
					calendarItemType = "User";
				}
				else if( target == "Global" ) {
					toID = "0";
					calendarItemType = "Global";
				}

				// Now that we've determined the target and id, we can call the ajax function
				loadBig(true);

				$.ajax({
					"url"		: "../api/CalendarItem/createCalendarItem.php",
					"dataType"	: "json",
					"data"		: {
							"ToID"	: toID,
							"CalendarItemType"	: calendarItemType,
							"DateStart"	: formatTimestamp( start ),
							"DateEnd"	: formatTimestamp( end ),
							"CalendarItemContent" : desc,
							"CalendarItemTitle"	: title
						},
					"success" : function(data) {
						loadBig(false);
						if( !data.Error ) {
							// refresh the calendar display
							DisplayCalendar();
							// clear the data from the form
							$('#calendarItemTitleInput, #calendarItemContentInput, #calendarItemTypeInput, #calendarSpecificInput, #startDateInput, #startDateInput, #endDateinput').val('');				

						}
						else {
							bootbox.alert( "Error: " + data.Error );
						}
					}
				});
			}

			else {
				bootbox.alert("Error: Start date needs to come before the end date");
				loadBig(false);
			}


		}
		catch(e) {
			bootbox.alert("Error: Invalid end date format. Make sure to use the datepicker icon.");
			loadBig(false);
		}

	}
	catch(e) {
		bootbox.alert("Error: Invalid start date format. Make sure to use the datepicker icon.");
		loadBig(false);
	}


}


// Choose whether or not to display the additional dropdown in the 'add calendar event' form
// If we do, populate it with the appropriate fields from the server
function showSpecificCalendar(e) {

	var type = $(e.target).val();

	$('#calendarSpecificInputDiv').hide(100);

	// show the additional dropdown
	if( type == "Project" || type == "User" ) {

		// first show loading spinner
		$('#calSpecificLoading').show(100);


		// Determine function to call
		var url = "";
		if( type == "Project" ) {
			// Now make the ajax function call
			$.ajax({
				"url"		: "../api/Project/getProjects.php",
				"dataType"	: "json",
				"success"	: function(data) {

					$('#calSpecificLoading').hide(100);

					if( !data.Error ) {
						// Show dropdown div
						$('#calendarSpecificInputDiv').show(100);

						// clear out old contents
						$('#calendarSpecificInput').html('');

						// set new contents
						for(var i = 0; i < data.length; i++) {
							$('<option/>')
								.attr('value', data[i].ProjectID )
								.text(data[i].ProjectName)
								.appendTo( $('#calendarSpecificInput') );
						};

					}
					else {
						bootbox.alert("Error: " + data.Error);
					}

				}
			});
		}	
		else if( type == "User" ) {
			$.ajax({
				"url"		: "../api/User/getUsers.php",
				"dataType"	: "json",
				"success"	: function(data) {

					$('#calSpecificLoading').hide(100);

					if( !data.Error ) {

						// Show dropdown div
						$('#calendarSpecificInputDiv').show(100);

						// clear out old contents
						$('#calendarSpecificInput').html('');

						// set new contents
						for(var i = 0; i < data.length; i++) {

							var nametext = data[i].UserName;
							if( data[i].FirstName && data[i].LastName ) nametext = data[i].FirstName + ' ' + data[i].LastName + ' (' + data[i].UserName + ')';	
							$('<option/>')
								.attr('value', data[i].UserID )
								.text(nametext)
								.appendTo( $('#calendarSpecificInput') );
						};
					}					
					else {
						bootbox.alert("Error: " + data.Error);
					}

				}
			});
		}
	}


}
