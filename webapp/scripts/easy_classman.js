// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.



// get the areas of focus and class terms
function showClassManagement() {


	$('#classList, #areaList').append('<td><img src="images/spinner-small.gif" alt="loading" /></td>');

	// Refersh the areas of focus and the Classes
	$.ajax({
		'url'		: '../api/AreaOfFocus/getAreasOfFocus.php',
		'dataType'	: 'json',
		'success'	: function(data) {
			$('#areaList').html('');

			if(!data.Error) {
				//console.log(data);
				// Loop through and list all areas
				for(var i = 0; i < data.length; i++ ) {


					(function(area) {
					var tr = $('<tr/>').appendTo($('#areaList')).css('marginTop','10px');

						tr.append('<td>'+ area.AreaOfFocusName + '</td>');

						$('<button/>')
							.appendTo(tr)
							.text('X')
							.attr('title','delete')
							.addClass('btn btn-danger')
							.click(function(){
								deleteArea(area.AreaOfFocusID);
							});
					})(data[i]);
	

				}
			}
			else {
				bootbox.alert('Error: '+data.Error);
			}
		}
	});


	// Refersh the areas of focus and the Classes
	$.ajax({
		'url'		: '../api/Class/getClasses.php',
		'dataType'	: 'json',
		'success'	: function(data) {
			$('#classList').html('');

			if(!data.Error) {
				//console.log(data);
				// Loop through and list all areas
				for(var i = 0; i < data.length; i++ ) {

					(function(mclass) {

						var tr = $('<tr/>').appendTo($('#classList')).css('marginTop','10px');

						tr.append('<td>'+ mclass.ClassTerm +  '</td>');
						tr.append('<td>'+ mclass.ClassYear +  '</td>');


						$('<button/>')
							.appendTo(tr)
							.text('X')
							.attr('title', 'delete')
							.addClass('btn btn-danger')
							.click(function(){
								deleteClass(mclass.ClassID);
							});

					})(data[i]);


				}
			}
			else {
				bootbox.alert('Error: '+data.Error);
			}
		}
	});	


	$.ajax({
		'url'		: '../api/MainMessage/getMainMessage.php',
		'dataType'	: 'json',
		'success'	: function(data) {
			 if(!data.Error) {
			 	$('#mainMessageInput').val(data.MainContent);
			 }
			 else {
			 	bootbox.alert('Error: ' + data.Error);
			 }
		}
	});

}


$('#createClass').click(function() {
	var year = $('#newClassYear').val();
	var term = $('#newClassTerm').val();

	if( year.length != 4 ) {
		bootbox.alert("Invalid year. Please enter year in the format (yyyy)");
		return;
	}

	loadBig(true);
	$.ajax({
		'url'		: '../api/Class/createClass.php',
		'dataType'	: 'json',
		'data'		: {
			ClassYear : year,
			ClassTerm : term
		},
		'success'	: function(data){
			loadBig(false);
			if(!data.Error) {
				bootbox.alert('Class created successfully');
				Navigate('Class-Management');
				// Get the class list
				getClasses();    				
			}
			else {
				bootbox.alert('Error: ' + data.Error);
			}
		}
	});

});



$('#createArea').click(function() {

	var name = $('#newAreaOfFocus').val();

	if( newAreaOfFocus.length > 1 ) {
		bootbox.alert("Invalid name. Please enter a valid area of focus name.");
		return;
	}

	loadBig(true);
	$.ajax({
		'url'		: '../api/AreaOfFocus/createAreaOfFocus.php',
		'dataType'	: 'json',
		'data'		: {
			AreaOfFocusName : name
		},
		'success'	: function(data){
			loadBig(false);
			if(!data.Error) {
				bootbox.alert('Area of focus created successfully');
				Navigate('Class-Management');

				// Get the areas of focus
				getAreasOfFocus(function() {});				
			}
			else {
				bootbox.alert('Error: ' + data.Error);
			}
		}
	});

});

$('#changeMessage').click(function() {
	var msg = $('#mainMessageInput').val();
	loadBig(true);
	$.ajax({
		'url'		: '../api/MainMessage/editMainMessage.php',
		'dataType'	: 'json',
		'data'		: {
			'MainContent'	: msg
		},
		'success'	: function(data) {
			loadBig(false);
			if(!data.Error) {
				showClassManagement();
			}
			else {
				bootbox.alert("Error: " + data.Error);
			}
		}
	})

});

// bring up a confirm box, and if confirmed, delete the class
function deleteClass(id) {
	bootbox.confirm("Are you sure you want to delete this class? All references to it will be deleted.", function(confirmed) {
		if(confirmed) {
			loadBig(true);
			$.ajax({
				'url'		: '../api/Class/deleteClass.php',
				'dataType'	: 'json',
				'data'		: {
					ClassID 	: id
				},
				'success'	: function(data) {
					loadBig(false);
					if(!data.Error) {
						bootbox.alert("Class deleted successfully.")
						Navigate('Class-Management');

						getClasses();

					}
					else {
						bootbox.alert('Error: ' + data.Error);
					}					
				}
			})
		}
	})
}

function deleteArea(id) {
	bootbox.confirm("Are you sure you want to delete this area of focus? All references to it will be deleted.", function(confirmed) {
		if(confirmed) {
			loadBig(true);
			$.ajax({
				'url'		: '../api/AreaOfFocus/deleteAreaOfFocus.php',
				'dataType'	: 'json',
				'data'		: {
					AreaOfFocusID 	: id
				},
				'success'	: function(data) {
					loadBig(false);
					if(!data.Error) {
						bootbox.alert("Area of Focus deleted successfully.")
						Navigate('Class-Management');

						getAreasOfFocus(function() {});		

					}
					else {
						bootbox.alert('Error: ' + data.Error);
					}					
				}
			})
		}
	})
}
