<?php

// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.

	require_once('../php_includes/openDbConn.php');
	include('../php_includes/utility_functions.php');


	// Only instructors can delete Ratings
	if( getUserType() == 3 ) {

		// We have to build the query sequentially because of all the optional Project input
		$values = "";

		if( isset( $_GET["ProjectRatingID"] )) {
			$values .= "ProjectRatingID=" . clean("ProjectRatingID", true, $con) . " AND ";
		} 

		if( isset( $_GET["ProjectID"] )) {
			$values .= "ProjectID=" . clean("ProjectID", true, $con) . " AND ";
		} 

		if( isset( $_GET["UserID"] )) {
			$values .= "UserID=".  clean("UserID", true, $con) . " AND ";
		} 

		// Format the ending and take out the commas that shouldn't be there
		if( $values != "") {
			
			// Build the query
			$values = substr($values, 0, strlen($values) - 5);
			$q = "DELETE FROM ProjectRating WHERE " . $values;

			// Execute
			$result = mysqli_query($con, $q);
			// Check to make sure the query went through
			if( $result )
				echo "{\"Success\" : true }";
			else
				errormsg("Invalid ProjectRatingID provided");

		}
		else {
			errormsg("No Parameters were specified");
		}	
	}
	else {
		errormsg("Must be an instructor to delete a ProjectRatingID.");

		exit;
	}	



?>