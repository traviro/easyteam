<?php

// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.

	require_once('../php_includes/openDbConn.php');
	include('../php_includes/utility_functions.php');




	// We have to build the query sequentially because of all the optional Project input
	$values = "";

	if( isset( $_GET["ProjectRatingID"] )) {
		$values .= "ProjectRatingID=" . clean("ProjectRatingID", true, $con) . " AND ";
	} 

	if( isset( $_GET["ProjectID"] )) {
		$values .= "ProjectID=" . clean("ProjectID", true, $con) . " AND ";
	} 

	if( isset( $_GET["UserID"] )) {
		$values .= "UserID=".  clean("UserID", true, $con) . " AND ";
	} 

	if( isset( $_GET["RatingVal"] )) {
		$values .= "RatingVal=".  clean("RatingVal", true, $con) . " AND ";
	} 	

	// Format the ending and take out the commas that shouldn't be there
	if( $values != "") {
		
		// Build the query
		$values = substr($values, 0, strlen($values) - 5);
		$q = "SELECT * FROM ProjectRating WHERE " . $values;

	}
	else {
		// Build the query
		$q = "SELECT * FROM ProjectRating";
	}



	if( isset( $_GET['ProjectRatingID'] ) ) {
		$ProjectRatingID = clean( "ProjectRatingID", false, $con );
		$q = "SELECT * FROM ProjectRating WHERE ProjectRatingID=$ProjectRatingID";
	}

	// Execute
	$result = mysqli_query($con, $q);

	// If the query was a success
	if( $result ) {
		// If there were results found
		if( mysqli_num_rows( $result) > 0) {


			$areas = array();

			while( $row = mysqli_fetch_array( $result ) ) {

				// Create a new array for this areaof focus
				$area = array(
					"ProjectRatingID" 	=> $row["ProjectRatingID"],
					"UserID" 			=> $row["UserID"],
					"ProjectID" 		=> $row["ProjectID"],
					"RatingDate" 		=> $row["RatingDate"],
					"RatingVal" 		=> $row["RatingVal"],
				);

				// Push it into the all classes array
				array_push($areas, $area);

			}

			// Echo our results
			echo json_encode($areas);

			// Close our connection and exit
			mysqli_close($con);

			exit;


		}
		else {
			errormsg("No ratings found.");
		}
	}
	else {
		errormsg("Invalid ID specified.");
	}


?>