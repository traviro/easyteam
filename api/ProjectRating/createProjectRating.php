<?php

// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.

	require_once('../php_includes/openDbConn.php');
	include('../php_includes/utility_functions.php');


	// Any logged in user can rate a project
	if( getUserType() >= 0 ) {


		// clean the variables
		$UserID = getUserID();
		$ProjectID = clean("ProjectID", true, $con);

		// Rating is an integer from 1-10
		$RatingVal = clean("RatingVal", true, $con);


		// Rating must be between 1 and 10
		if( $RatingVal >= 1 && $RatingVal <= 10) {


			// Let's check to see if a rating for this user and project already exists
			$q = "SELECT ProjectRatingID FROM ProjectRating WHERE UserID=$UserID AND ProjectID=$ProjectID";
			$r = mysqli_query( $con, $q );

			if( mysqli_num_rows($r) > 0 ) {
				$q = "UPDATE ProjectRating SET RatingVal=$RatingVal WHERE UserID=$UserID AND ProjectID=$ProjectID";	
			}
			else {
				// Build query 
				$q = "INSERT INTO ProjectRating (UserID, ProjectID, RatingVal) VALUES('$UserID', '$ProjectID', '$RatingVal')";
			}



			// Execute
			$r = mysqli_query( $con, $q );


			// If the query was successful go ahead and return the class ID
			if ( $r ) {

				// Find out what the ClassID of thihs class is
				$q = "SELECT ProjectRatingID FROM ProjectRating ORDER BY ProjectRatingID DESC LIMIT 1";
				// Execute
				$r = mysqli_query( $con , $q );
				$row = mysqli_fetch_array( $r );
				$ProjectRatingID = $row["ProjectRatingID"];

				// echo the class id
				echo "{\"ProjectRatingID\" : $ProjectRatingID}";


				mysqli_close($con);

				exit;
			}
			else {
				errormsg("Invalid parameters given to the rating");
			}		
		}
		else {
			errormsg("Rating must be from 1 to 10");
		}	
	}
	else {
		errormsg("Must be logged in to rate a project");
	}


?>