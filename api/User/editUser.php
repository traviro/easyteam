<?php

// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.

	require_once('../php_includes/openDbConn.php');
	include('../php_includes/utility_functions.php');

	if(isset( $_GET["UserID"] )) {

		// Only instructors and the target user can edit
		if( getUserType() == 3 || getUserID() == $_GET["UserID"]) {

			// We have to build the query sequentially because of all the optional user input

			$values = "";

			if( isset( $_GET["ClassID"] )) {
				$values .= "ClassID=" . clean("ClassID", true, $con) . ", ";
			} 

			if( isset( $_GET["ProjectID"] )) {
				$values .= "ProjectID=".  clean("ProjectID", true, $con) . ", ";
			} 

			if( isset( $_GET["AreaOfFocusID"] )) {
				$values .= "AreaOfFocusID=" .  clean("AreaOfFocusID", true, $con) . ", ";
			} 

			if( isset( $_GET["UserName"] )) {
				$values .= "UserName='" . clean("UserName", true, $con) . "', ";
			} 

			if( isset( $_GET["Password"] )) {
				$values .= "Password='" . md5( clean("Password", true, $con) ) . "', ";
			} 

			if( isset( $_GET["FirstName"] )) {
				$values .= "FirstName='" . clean("FirstName", true, $con) . "', ";
			} 		

			if( isset( $_GET["LastName"] )) {
				$values .= "LastName='" . clean("LastName", true, $con) . "', ";
			} 	

			if( isset( $_GET["AvailabilityMatrix"] )) {
				$values .= "AvailabilityMatrix='" . clean("AvailabilityMatrix", true, $con) . "', ";
			} 	

			if( isset( $_GET["UserType"] )) {
				$values .= "UserType=". clean("UserType", true, $con) . ", ";
			} 		

			if( isset( $_GET["AboutMe"] )) {
				$values .= "AboutMe='" . clean("AboutMe", true, $con) . "', ";
			} 		

			if( isset( $_GET["PhoneNumber"] )) {
				$values .= "PhoneNumber='" . clean("PhoneNumber", true, $con) . "', ";
			} 		

			if( isset( $_GET["PortfolioURL"] )) {
				$values .= "PortfolioURL='" . clean("PortfolioURL", true, $con) . "', ";
			} 		

			if( isset( $_GET["Email"] )) {
				$values .= "Email='" . clean("Email", true, $con) . "', ";
			} 								

			// clean the UserID
			$UserID = clean("UserID", true, $con);	

			// Format the ending and take out the commas that shouldn't be there
			if( $values != "") {
				$values = substr($values, 0, strlen($values) - 2);
			}
			else {
				$values = "UserID=$UserID";;
			}	

			// Now we can finally build the query
			$q = "UPDATE User SET " . $values . " WHERE UserID=$UserID";
			// Exectue
			$r = mysqli_query( $con, $q );

			// If the user creation request completed successfully, lets find out what the userID is and return it.
			if( $r ) {

				// Find out what the UserID of thihs class is
				$q = "SELECT UserID FROM User ORDER BY UserID DESC LIMIT 1";
				// Execute
				$r = mysqli_query( $con , $q );
				$row = mysqli_fetch_array( $r );
				$UserID = $row["UserID"];


				// echo the class id
				echo "{\"Success\" : true}";
				
			}
			// Otherwise we show an error.
			else {
				errormsg("Unable to edit user. Make sure all fields were filled out correctly.");
			}
		}
		else {
			errormsg("You do not have permission to edit this user.");
		}
	}
	else {
		errormsg("No UserID was specified.");
	}


?>