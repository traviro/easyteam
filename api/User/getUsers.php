<?php

// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.

	require_once('../php_includes/openDbConn.php');
	include('../php_includes/utility_functions.php');

	$Basic = clean("Basic", false, $con);

	// We have to build the query sequentially because of all the optional user input
	$values = "";

	if( isset( $_GET["UserID"] )) {
		$values .= "UserID=" . clean("UserID", true, $con) . " AND ";
	} 

	if( isset( $_GET["ClassID"] )) {
		$values .= "ClassID=" . clean("ClassID", true, $con) . " AND ";
	} 

	if( isset( $_GET["ProjectID"] )) {
		$values .= "ProjectID=".  clean("ProjectID", true, $con) . " AND ";
	} 

	if( isset( $_GET["AreaOfFocusID"] )) {
		$values .= "AreaOfFocusID=" .  clean("AreaOfFocusID", true, $con) . " AND ";
	} 

	if( isset( $_GET["UserName"] )) {
		$values .= "UserName='" . clean("UserName", true, $con) . "' AND ";
	} 

	if( isset( $_GET["Password"] )) {
		$values .= "Password='" . clean("Password", true, $con) . "' AND ";
	} 

	if( isset( $_GET["FirstName"] )) {
		$values .= "FirstName='" . clean("FirstName", true, $con) . "' AND ";
	} 		

	if( isset( $_GET["LastName"] )) {
		$values .= "LastName='" . clean("LastName", true, $con) . "' AND ";
	} 	

	if( isset( $_GET["AvailabilityMatrix"] )) {
		$values .= "AvailabilityMatrix='" . clean("AvailabilityMatrix", true, $con) . "' AND ";
	} 	

	if( isset( $_GET["UserType"] )) {
		$values .= "UserType=". clean("UserType", true, $con) . " AND ";
	} 		

	if( isset( $_GET["AboutMe"] )) {
		$values .= "AboutMe='" . clean("AboutMe", true, $con) . "' AND ";
	} 		
	if( isset( $_GET["PhoneNumber"] )) {
		$values .= "PhoneNumber='" . clean("PhoneNumber", true, $con) . "' AND ";
	} 	

	if( isset( $_GET["PortfolioURL"] )) {
		$values .= "PortfolioURL='" . clean("PortfolioURL", true, $con) . "' AND ";
	} 	

	if( isset( $_GET["Email"] )) {
		$values .= "Email='" . clean("Email", true, $con) . "' AND ";
	} 				

	// Format the ending and take out the commas that shouldn't be there
	if( $values != "") {
		$values = substr($values, 0, strlen($values) - 5);
		$q = "SELECT * FROM USER WHERE " . $values;
	}
	else {
		$q = "Select * FROM USER";
	}	
	$result = mysqli_query( $con, $q );


	if( $result ) {
		// Initialize classes array
		$allUsers = array();

		while( $row = mysqli_fetch_array( $result ) ) {

			// Create a new array for this class
			if( $Basic ) {
				$user = array(
					"UserID" => $row["UserID"],
					"UserName" => $row["UserName"],				
					"FirstName" => $row["FirstName"],
					"LastName" => $row["LastName"],				
					"ClassID" => $row["ClassID"],
					"ProjectID" => $row["ProjectID"],
					"UserType" => $row["UserType"],
				);
			}
			else {
				$user = array(
					"UserID" => $row["UserID"],
					"UserName" => $row["UserName"],				
					"FirstName" => $row["FirstName"],
					"LastName" => $row["LastName"],				
					"ClassID" => $row["ClassID"],
					"ProjectID" => $row["ProjectID"],
					"AreaOfFocusID" => $row["AreaOfFocusID"],
					"Password" => $row["Password"],
					"AvailabilityMatrix" => $row["AvailabilityMatrix"],
					"UserType" => $row["UserType"],
					"AboutMe" => $row["AboutMe"],
					"PhoneNumber" => $row["PhoneNumber"],
					"PortfolioURL" => $row["PortfolioURL"],
					"Email" => $row["Email"],
				);
			}

			// Push it into the all classes array
			array_push($allUsers, $user);

		}

		// Echo our results
		echo json_encode($allUsers);

		// Close our connection and exit
		mysqli_close($con);

		exit;
	}
	else {
		errormsg("Invalid search parameters.");
	}



?>