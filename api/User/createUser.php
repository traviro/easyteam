<?php

// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.

	require_once('../php_includes/openDbConn.php');
	include('../php_includes/utility_functions.php');

	// Only instructors can create users
	if( getUserType() == 3 ) {


		// We have to build the query sequentially because of all the optional user input

		$colNames = "(";
		$values = "(";

		if( isset( $_GET["ClassID"] )) {
			$colNames .= "ClassID, ";
			$values .= clean("ClassID", true, $con) . ", ";
		} 

		if( isset( $_GET["ProjectID"] )) {
			$colNames .= "ProjectID, ";
			$values .= clean("ProjectID", true, $con) . ", ";
		} 

		if( isset( $_GET["AreaOfFocusID"] )) {
			$colNames .= "AreaOfFocusID, ";
			$values .= clean("AreaOfFocusID", true, $con) . ", ";
		} 

		if( isset( $_GET["UserName"] )) {
			$colNames .= "UserName, ";
			$values .= "'" . clean("UserName", true, $con) . "', ";
		} 

		if( isset( $_GET["Password"] )) {
			$colNames .= "Password, ";
			$values .= "'" . md5( clean("Password", true, $con) ) . "', ";
		} 

		if( isset( $_GET["FirstName"] )) {
			$colNames .= "FirstName, ";
			$values .= "'" . clean("FirstName", true, $con) . "', ";
		} 		

		if( isset( $_GET["LastName"] )) {
			$colNames .= "LastName, ";
			$values .= "'" . clean("LastName", true, $con) . "', ";
		} 	

		if( isset( $_GET["AvailabilityMatrix"] )) {
			$colNames .= "AvailabilityMatrix, ";
			$values .= "'" . clean("AvailabilityMatrix", true, $con) . "', ";
		} 	

		if( isset( $_GET["UserType"] )) {
			$colNames .= "UserType, ";
			$values .= clean("UserType", true, $con) . ", ";
		} 		

		if( isset( $_GET["AboutMe"] )) {
			$colNames .= "AboutMe, ";
			$values .= "'" . clean("AboutMe", true, $con) . "', ";
		} 		

		if( isset( $_GET["PhoneNumber"] )) {
			$colNames .= "PhoneNumber, ";
			$values .= "'" . clean("PhoneNumber", true, $con) . "', ";
		} 	

		if( isset( $_GET["PortfolioURL"] )) {
			$colNames .= "PortfolioURL, ";
			$values .= "'" . clean("PortfolioURL", true, $con) . "', ";
		}

		if( isset( $_GET["Email"] )) {
			$colNames .= "Email, ";
			$values .= "'" . clean("Email", true, $con) . "', ";
		} 				 				


		// Format the ending and take out the commas that shouldn't be there
		if( $colNames != "(" && $values != "(") {
			$colNames = substr($colNames, 0, strlen($colNames) - 2) . ")";
			$values = substr($values, 0, strlen($values) - 2) . ")";
		}
		else {
			$colNames = "()";
			$values = "()";
		}			



		// Now we can finally build the query
		$q = "INSERT INTO User " . $colNames . " VALUES " . $values;
		// Exectue
		$r = mysqli_query( $con, $q );

		// If the user creation request completed successfully, lets find out what the userID is and return it.
		if( $r ) {

			// Find out what the UserID of thihs class is
			$q = "SELECT UserID FROM User ORDER BY UserID DESC LIMIT 1";
			// Execute
			$r = mysqli_query( $con , $q );
			$row = mysqli_fetch_array( $r );
			$UserID = $row["UserID"];


			// echo the class id
			echo "{\"UserID\" : $UserID}";

		}
		// Otherwise we show an error.
		else {
			errormsg("Unable to create user. Check to make sure all fields were entered correctly.");
		}



	}
	else {
		errormsg("You do not have permission to create users.");
	}

?>