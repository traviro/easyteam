<?php

// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.

	require_once('../php_includes/openDbConn.php');
	include('../php_includes/utility_functions.php');


	// Only instructors can delete users
	if( getUserType() == 3 ) {

		$UserID = clean( "UserID", true, $con );


		// First let's remove all references to this project in the user table and such
		$q = 'UPDATE Project SET SponsorID = NULL WHERE SponsorID='.$UserID;
		$result = mysqli_query($con, $q);

		$q = 'UPDATE Project SET AdvocateID = NULL WHERE AdvocateID=' . $UserID;
		
		$result = mysqli_query($con, $q);		


		// Build query
		$q = "DELETE FROM User WHERE UserID=$UserID";
		// Execute
		$result = mysqli_query($con, $q);

		$q = "DELETE FROM ProjectRating WHERE UserID=$UserID";
		// Execute
		$result = mysqli_query($con, $q);

		$q = "DELETE FROM ProjectApplication WHERE UserID=$UserID";
		// Execute
		$result = mysqli_query($con, $q);

		$q = "DELETE FROM Message WHERE UserID=$UserID";
		// Execute
		$result = mysqli_query($con, $q);

		$q = "DELETE FROM CalendarItem WHERE UserID=$UserID";
		// Execute
		$result = mysqli_query($con, $q);								


		// Check to make sure the query went through
		if( $result )
			echo "{\"Success\" : true }";
		else
			errormsg("Invalid UserID provided");

	}
	else {
		errormsg("Must be an instructor to delete a User.");

		exit;
	}		

?>