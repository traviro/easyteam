<?php

// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.


	// Create connection
	$con=mysqli_connect("localhost","root","","easyteamapi");

	// Check connection
	if (mysqli_connect_errno($con))	{
		echo "{Error: '" . mysqli_connect_error() . "'}";
		exit;
	}
?>