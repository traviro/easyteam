<?php

// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.

ini_set('session.bug_compat_warn', 0);
ini_set('session.bug_compat_42', 0);

	session_start();


	// Clean user input to protect from mysql injection
	function clean($varname, $required, $con) {

		if( isset( $_GET[ $varname ] ) ) {

			return trim( mysqli_real_escape_string($con, $_GET[ $varname ] ) );

		}
		else if($required) {
			errormsg($varname . " was not specified.");
		}
		else {
			return "";
		}

		
	}

		// Clean user input to protect from mysql injection
	function cleanPOST($varname, $required, $con) {

		if( isset( $_POST[ $varname ] ) ) {

			return trim( mysqli_real_escape_string($con, $_GET[ $varname ] ) );

		}
		else if($required) {
			errormsg($varname . " was not specified.");
		}
		else {
			return "";
		}

		
	}


	// Return the user's authorization lvel
	// 0 = Logged out user
	// 1 = Student
	// 2 = Sponsor
	// 3 = Instructor
	function getUserType() {

		// Temporarily just return 3 (Instructor level permissions)
		return 3;
		//

		if( !isset( $_SESSION['UserType'] )  ) {
			return 0;
		}
		else {
			return  $_SESSION['UserType'];
		}
	}


	// Check if the logged in user is a specified UserID
	function getUserID() {

		// If user isn't logged in return -1
		if( !isset( $_SESSION['UserID'] )  ) {
			return -1;
		}
		else {
			return  $_SESSION['UserID'];
		}
	}


	function errormsg($msg) {
		$error = array(
			"Error" => $msg,
		);
		echo json_encode($error);
		exit;
	}


?>