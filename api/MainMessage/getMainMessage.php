<?php
// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.


	require_once('../php_includes/openDbConn.php');
	include('../php_includes/utility_functions.php');




	$q = "SELECT * FROM MainMessage WHERE MainID=1";
	$result = mysqli_query( $con, $q);


	if( mysqli_num_rows( $result ) == 1) {

		$row = mysqli_fetch_array($result);


		$arr = array(
				'MainID' => $row['MainID'],
				'MainContent' => $row['MainContent'],
			);

		echo json_encode($arr);

	}
	else {
		errormsg("Could not retreive the main message");
	}



?>