<?php

// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.

	require_once('../php_includes/openDbConn.php');
	include('../php_includes/utility_functions.php');

	// Only instructors can create new areas of focus
	if( getUserType() == 3 ) {

		// Get and clean the variabels
		$AreaOfFocusName = clean( "AreaOfFocusName", true, $con);
	
		// Build query 
		$q = "INSERT INTO AreaOfFocus (AreaOfFocusName) VALUES('$AreaOfFocusName')";
		// Execute
		$r = mysqli_query( $con, $q );


		// If the query was successful go ahead and return the class ID
		if ( $r ) {

			// Find out what the ClassID of thihs class is
			$q = "SELECT AreaOfFocusID FROM AreaOfFocus ORDER BY AreaOfFocusID DESC LIMIT 1";
			// Execute
			$r = mysqli_query( $con , $q );
			$row = mysqli_fetch_array( $r );
			$AreaOfFocusID = $row["AreaOfFocusID"];

			// echo the class id
			echo "{\"AreaOfFocusID\" : $AreaOfFocusID}";


			mysqli_close($con);

			exit;
		}
		else {
			errormsg("Invalid parameters given to create the Area of focus");
		}			


	}
	else {
		errormsg("Must be an instructor to create an area of focus.");
		exit;
	}


?>