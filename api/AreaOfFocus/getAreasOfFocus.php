<?php

// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.

	require_once('../php_includes/openDbConn.php');
	include('../php_includes/utility_functions.php');


	// Build the query
	$q = "SELECT * FROM AreaOfFocus";

	if( isset( $_GET['AreaOfFocusID'] ) ) {
		$AreaOfFocusID = clean( "AreaOfFocusID", false, $con );
		$q = "SELECT * FROM AreaOfFocus WHERE AreaOfFocusID=$AreaOfFocusID";
	}

	// Execute
	$result = mysqli_query($con, $q);

	// If the query was a success
	if( $result ) {
		// If there were results found
		if( mysqli_num_rows( $result) > 0) {


			$areas = array();

			while( $row = mysqli_fetch_array( $result ) ) {

				// Create a new array for this areaof focus
				$area = array(
					"AreaOfFocusID" 	=> $row["AreaOfFocusID"],
					"AreaOfFocusName" 	=> $row["AreaOfFocusName"],
				);

				// Push it into the all classes array
				array_push($areas, $area);

			}

			// Echo our results
			echo json_encode($areas);

			// Close our connection and exit
			mysqli_close($con);

			exit;


		}
		else {
			errormsg("No Areas found.");
		}
	}
	else {
		errormsg("Invalid ID specified.");
	}

?>