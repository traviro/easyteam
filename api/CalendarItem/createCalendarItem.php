<?php

// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.


	// CalendarItem Types:
	// Project
	// User
	// Global

	require_once('../php_includes/openDbConn.php');
	include('../php_includes/utility_functions.php');

	// Only logged in users can create a CalendarItem
	if( getUserType() > 0 ) {

		// Get and clean the variabels
		$CalendarItemType = clean( "CalendarItemType", true, $con);
		$CalendarItemTitle = clean( "CalendarItemTitle", true, $con);
		$CalendarItemContent = clean( "CalendarItemContent", true, $con);
		$DateStart 	= clean( "DateStart", true, $con);
		$DateEnd 	= clean( "DateEnd", true, $con);
		$ToID 		= clean( "ToID", true, $con);
		$UserID 	= getUserID();


		// Only allow accepted CalendarItem types
		if( $CalendarItemType == "Project" || $CalendarItemType == "User" || $CalendarItemType == "Class" || $CalendarItemType == "Global") {

			// Build query 
			$q = "INSERT INTO CalendarItem (CalendarItemType, CalendarTitle, CalendarContent, UserID, ToID, DateStart, DateEnd) VALUES('$CalendarItemType', '$CalendarItemTitle', '$CalendarItemContent', $UserID, $ToID, '$DateStart', '$DateEnd')";
			
			//echo $q; exit;

			// Execute
			$r = mysqli_query( $con, $q );


			// If the query was successful go ahead and return the class ID
			if ( $r ) {


				// Find out what the ClassID of thihs class is
				$q = "SELECT CalendarItemID FROM CalendarItem ORDER BY CalendarItemID DESC LIMIT 1";
				// Execute
				$r = mysqli_query( $con , $q );
				$row = mysqli_fetch_array( $r );
				$CalendarItemID = $row["CalendarItemID"];

				// echo the class id
				echo "{\"CalendarItemID\" : $CalendarItemID}";


				mysqli_close($con);

				exit;
			}
			else {
				errormsg("Invalid parameters given to create the CalendarItem");
			}			

		}
		else {
			errormsg("Unrecognized CalendarItem Type");
		}
	}
	else {
		errormsg("Must be an instructor to create an area of focus.");
		exit;
	}


?>