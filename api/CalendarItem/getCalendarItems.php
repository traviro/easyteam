<?php

// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.

	require_once('../php_includes/openDbConn.php');
	include('../php_includes/utility_functions.php');


	// We have to build the query sequentially because of all the optional Project input
	$values = "";

	if( isset( $_GET["CalendarItemID"] )) {
		$values .= "CalendarItemID=" . clean("CalendarItemID", true, $con) . " AND ";
	} 

	if( isset( $_GET["UserID"] )) {
		$values .= "UserID=" . clean("UserID", true, $con) . " AND ";
	} 

	if( isset( $_GET["ToID"] )) {
		$values .= "ToID=".  clean("ToID", true, $con) . " AND ";
	} 

	if( isset( $_GET["CalendarItemType"] )) {
		$values .= "CalendarItemType='" .  clean("CalendarItemType", true, $con) . "' AND ";
	} 


	// Format the ending and take out the commas that shouldn't be there
	if( $values != "") {
		$values = substr($values, 0, strlen($values) - 5);
		$q = "SELECT * FROM CalendarItem WHERE " . $values;
	}
	else {
		$q = "Select * FROM CalendarItem";
	}	
	$result = mysqli_query( $con, $q );


	// If the query was a success
	if( $result ) {
		// If there were results found
		if( mysqli_num_rows( $result) > 0) {


			$areas = array();

			while( $row = mysqli_fetch_array( $result ) ) {

				// Create a new array for this areaof focus
				$area = array(
					"CalendarItemID" 	=> $row["CalendarItemID"],
					"UserID" 		=> $row["UserID"],
					"ToID" 			=> $row["ToID"],
					"CalendarItemType" 	=> $row["CalendarItemType"],
					"DateStart" 	=> $row["DateStart"],
					"DateEnd" 	=> $row["DateEnd"],
					"CalendarItemTitle" 	=> $row["CalendarTitle"],
					"CalendarItemContent" 	=> $row["CalendarContent"],
				);

				// Push it into the all classes array
				array_push($areas, $area);

			}

			// Echo our results
			echo json_encode($areas);

			// Close our connection and exit
			mysqli_close($con);

			exit;


		}
		else {
			echo "[]";
			exit;
		}
	}
	else {
		errormsg("Invalid ID specified.");
	}

?>