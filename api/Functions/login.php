<?php

// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.


	require_once('../php_includes/openDbConn.php');
	include('../php_includes/utility_functions.php');


	// We are using POST so the password won't show up in a url string

	$UserName = clean("UserName", true, $con);
	$Password = md5( clean("Password", true, $con) );


	// build query
	$q = "SELECT * FROM User WHERE UserName='$UserName' AND Password='$Password'";

	// Execute
	$r = mysqli_query( $con, $q );

	if( $r ) {

		if( mysqli_num_rows($r) == 1 ) {

			$row = mysqli_fetch_array($r);
			$info  = array();
			
			// Set session variables
			$_SESSION["ClassID"] 			= $row["ClassID"];
			$_SESSION["ProjectID"] 			= $row["ProjectID"];
			$_SESSION["AreaOfFocusID"] 		= $row["AreaOfFocusID"];
			$_SESSION["UserName"] 			= $row["UserName"];
			$_SESSION["UserType"] 			= $row["UserType"];
			$_SESSION["AvailabilityMatrix"]	= $row["AvailabilityMatrix"];
			$_SESSION["AboutMe"]			= $row["AboutMe"];
			$_SESSION["UserID"]				= $row["UserID"];
			$_SESSION["FirstName"]			= $row["FirstName"];
			$_SESSION["LastName"]			= $row["LastName"];
			$_SESSION["Password"]			= $row["Password"];
			$_SESSION["PhoneNumber"]		= $row["PhoneNumber"];
			$_SESSION["PortfolioURL"]		= $row["PortfolioURL"];
			$_SESSION["Email"]		= $row["Email"];

			// Set variables to return
			$info["ClassID"] 			= $row["ClassID"];
			$info["ProjectID"] 			= $row["ProjectID"];
			$info["AreaOfFocusID"] 		= $row["AreaOfFocusID"];
			$info["UserName"] 			= $row["UserName"];
			$info["UserType"] 			= $row["UserType"];
			$info["AvailabilityMatrix"]	= $row["AvailabilityMatrix"];
			$info["AboutMe"]			= $row["AboutMe"];
			$info["UserID"]				= $row["UserID"];
			$info["FirstName"]			= $row["FirstName"];
			$info["LastName"]			= $row["LastName"];	
			$info["Password"]			= $row["Password"];	
			$info["PhoneNumber"]		= $row["PhoneNumber"];	
			$info["PortfolioURL"]		= $row["PortfolioURL"];
			$info["Email"]		= $row["Email"];



			echo json_encode($info);


		}
		else {
			errormsg("Username or password incorrect");
		}

	}
	else {
		errormsg("Invalid login parameters");
	}


?>