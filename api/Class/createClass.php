<?php

// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.

	require_once('../php_includes/openDbConn.php');
	include('../php_includes/utility_functions.php');

	// Only instructors can create classes
	if( getUserType() == 3 ) {

		// Get and clean the variabels
		$ClassYear = clean( "ClassYear", true, $con);
		$ClassTerm = clean( "ClassTerm", true, $con);
		$ClassStatus = 0;

		// Build query 
		$q = "INSERT INTO Class (ClassTerm, ClassYear, ClassStatus) VALUES('$ClassTerm', '$ClassYear', $ClassStatus)";
		// Execute
		$r = mysqli_query( $con, $q );

		// If the query was successful go ahead and return the class ID
		if ( $r ) {

			// Find out what the ClassID of thihs class is
			$q = "SELECT ClassID FROM Class ORDER BY ClassID DESC LIMIT 1";
			// Execute
			$r = mysqli_query( $con , $q );
			$row = mysqli_fetch_array( $r );
			$ClassID = $row["ClassID"];


			// echo the class id
			echo "{\"ClassID\" : $ClassID}";


			mysqli_close($con);

			exit;
		}
		else {
			errormsg("Invalid parameters given to create the class");
		}	

	}
	else {
		errormsg("Must be an instructor to create a new class.");

		exit;
	}



?>