<?php

// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.

	require_once('../php_includes/openDbConn.php');
	include('../php_includes/utility_functions.php');

	// Only instructors can create classes
	if( getUserType() == 3 ) {


		// Get the class id from the user
		$ClassID = clean("ClassID", true, $con);

		// First we need to check if the ClassID we were given was valid
		// Build the query
		$q = "SELECT ClassID FROM Class WHERE ClassID=$ClassID LIMIT 1";
		//Execute
		$r = mysqli_query( $con, $q );	

		// If we found the Class, continue
		if( mysqli_num_rows( $r ) == 1 ) {

			// Set all the ClassStatuses to inactive first
			$q = "UPDATE Class SET ClassStatus=0";
			// Execute
			$r = mysqli_query( $con, $q );

			// Set the specified class status to 1
			$q = "UPDATE CLASS SET ClassStatus=1 WHERE ClassID=$ClassID";
			// Execute
			$r = mysqli_query( $con, $q );

			// Echo a success
			echo "{\"Success\" : true }";
			mysqli_close($con);
			exit;

		}
		else {
			errormsg("Class does not exist or ClassID was invalid.");
		}


	}
	else {
		errormsg("Must be an instructor to set the active class.");
	}		


?>