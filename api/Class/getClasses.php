<?php

// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.

	require_once('../php_includes/openDbConn.php');
	include('../php_includes/utility_functions.php');


	//Build the query
	$q = "SELECT * FROM Class";
	$result = mysqli_query( $con, $q );


	// Initialize classes array
	$allClasses = array();

	while( $row = mysqli_fetch_array( $result ) ) {

		// Create a new array for this class
		$class = array(
			"ClassID" => $row["ClassID"],
			"ClassYear" => $row["ClassYear"],
			"ClassTerm" => $row["ClassTerm"],
			"ClassStatus" => $row["ClassStatus"],
		);

		// Push it into the all classes array
		array_push($allClasses, $class);

	}

	// Echo our results
	echo json_encode($allClasses);

	// Close our connection and exit
	mysqli_close($con);

	exit;


?>