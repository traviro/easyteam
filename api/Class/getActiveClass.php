<?php

// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.

	require_once('../php_includes/openDbConn.php');
	include('../php_includes/utility_functions.php');

	//Build the query
	$q = "SELECT * FROM Class WHERE ClassStatus=1 LIMIT 1";
	//Execute
	$r = mysqli_query( $con, $q );

	// If we found a result, fetch it
	if( mysqli_num_rows($r) == 1 ) {

		$row = mysqli_fetch_array( $r );

		// store row into an array
		$array = array(
			"ClassID" => $row["ClassID"],
			"ClassYear" => $row["ClassYear"],
			"ClassTerm" => $row["ClassTerm"],
			"ClassStatus" => $row["ClassStatus"],
		);

		// output that array
		echo json_encode( $array );

		// close the connection
		mysqli_close($con);

		exit;

	}
	else {
		errormsg("No active class was found.");
	}




?>