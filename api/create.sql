CREATE TABLE Class(
	ClassID   int NOT NULL AUTO_INCREMENT,
	ClassYear varchar(4),
	ClassTerm varchar(8),
	ClassStatus int,
	PRIMARY KEY(ClassID)
);

CREATE TABLE AreaOfFocus(
	AreaOfFocusID   int NOT NULL AUTO_INCREMENT,
	AreaOfFocusName varchar(128),
	PRIMARY KEY(AreaOfFocusID)
);

CREATE TABLE User(
	UserID int NOT NULL AUTO_INCREMENT,
	ClassID int
		REFERENCES Class(ClassID),
	ProjectID int
		REFERENCES Project(ProjectID),
	AreaOfFocusID int
		REFERENCES AreaOfFocus(AreaOfFocusID),
	UserName varchar(32),
	UserType int NOT NULL DEFAULT 1,
	Password varchar(32),
	FirstName varchar(32),
	LastName varchar(32),
	AvailabilityMatrix varchar(256),
	AboutMe varchar(1024),
	PhoneNumber varchar(10),
	PorfolioURL varchar(128),
	Email varchar(128),
	PRIMARY KEY(UserID)
);

CREATE TABLE Project(
	ProjectID   int NOT NULL AUTO_INCREMENT,
	ClassID int
		REFERENCES Class(ClassID),
	SponsorID int
		REFERENCES User(UserID),
	AdvocateID int
		REFERENCES User(UserID),
	ProjectName varchar(128),
	ProjectDate Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Description varchar(2048),					
	PRIMARY KEY(ProjectID)
);

CREATE TABLE ProjectApplication(
	ProjectApplicationID int NOT NULL AUTO_INCREMENT,
	ProjectID int
		REFERENCES Project(ProjectID),
	UserID int
		REFERENCES User(UserID),
	ApplicationDate TimeStamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	ApplicationPath		varchar(256),
	ApplicationStatus 	int NOT NULL DEFAULT 0,
	PRIMARY KEY(ProjectApplicationID)
);

CREATE TABLE ProjectRating(
	ProjectRatingID int NOT NULL AUTO_INCREMENT,
	UserID int
		REFERENCES User(UserID),
	ProjectID int
		REFERENCES Project(ProjectID),
	RatingDate TimeStamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	RatingVal int,
	PRIMARY KEY(ProjectRatingID)
);

CREATE TABLE Message(
	MessageID int NOT NULL AUTO_INCREMENT,
	UserID int
		REFERENCES User(UserID),
	ToID int,
	MessageType varchar(16),
	MessageDate TimeStamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	MessageTitle varchar(128),
	MessageContent varchar(512),
	MessageSeen int NOT NULL Default 0,
	PRIMARY KEY(MessageID)
);

CREATE TABLE CalendarItem(
	CalendarItemID int NOT NULL AUTO_INCREMENT,
	UserID int
		REFERENCES User(UserID),
	ToID int,
	CalendarItemType varchar(16),
	DateStart TimeStamp,
	DateEnd TimeStamp,
	CalendarTitle varchar(128),
	CalendarContent varchar(512),
	PRIMARY KEY(CalendarItemID)
);

CREATE TABLE Link(
	LinkID int NOT NULL AUTO_INCREMENT,
	PrimaryCategory varchar(128),
	URL varchar(512),
	LinkDescription varchar(1024),
	PRIMARY KEY(LinkID)
);

CREATE TABLE MainMessage(
	MainID int NOT NULL AUTO_INCREMENT,
	MainContent varchar(32768),
	PRIMARY KEY(MainID)	
);

INSERT INTO  MainMessage (MainID, MainContent) VALUES ( '1', NULL );