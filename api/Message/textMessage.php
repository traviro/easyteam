<?php

// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.

	require_once('../php_includes/openDbConn.php');
	include('../php_includes/utility_functions.php');





	$carriers = array(
			"Verizon" => "@vtext.com",
			"ATT" => "@txt.att.net",
			"TMobile" => "@tmomail.net",
			"Sprint1" => "@messaging.sprintpcs.com",
			"Sprint2" => "@sprintpcs.com",
		);







		// Only logged in users can create a message
	if( getUserType() > 0 ) {


		// Get and clean the variabels
		$MessageType = clean("MessageType", true, $con);
		$MessageTitle = clean("MessageTitle", true, $con);
		$MessageContent = clean("MessageContent", true, $con);

		$UserID =  clean("UserID", true, $con);
		$Sender = "Travis";


		// Build the query
		$q = "SELECT PhoneNumber FROM User WHERE UserID=$UserID";

		// Execute
		$res = mysqli_query( $con, $q );


		if( $res ) {

			if( mysqli_num_rows( $res ) == 1 ) {

				$row = mysqli_fetch_array( $res );

				if(strlen($row['PhoneNumber']) == 10) {

					if(strlen($MessageContent) < 110) {

						$pn = $row['PhoneNumber'];

						foreach( $carriers as $carrier => $address)	{
							mail($pn . $address, $MessageTitle, $MessageContent, "From: ".$Sender."@cgt411.net" . "\r\n");
						}

					}
					else {
						errormsg("Message must be 110 characters or less.");
					}
				}
				else {
					errormsg("Invalid phone number");
				}
			}
			else {
				errormsg("User not found.");
			}

		}
		else {
			errormsg("Invalid query.");
		}
	}
	else {
		errormsg("Insufficient priveleges to send a text message");
	}

?>