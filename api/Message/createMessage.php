<?php

// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.


	// Message Types:
	// Project
	// User
	// Global

	require_once('../php_includes/openDbConn.php');
	include('../php_includes/utility_functions.php');

	// Only logged in users can create a message
	if( getUserType() > 0 ) {

		// Get and clean the variabels
		$MessageType = clean( "MessageType", true, $con);
		$MessageTitle = clean( "MessageTitle", true, $con);
		$MessageContent = clean( "MessageContent", true, $con);
		$ToID = clean( "ToID", true, $con);
		$UserID = getUserID();


		// Only allow accepted message types
		if( $MessageType == "Project" || $MessageType == "User" || $MessageType == "Class" || $MessageType == "Global") {

			// Build query 
			$q = "INSERT INTO Message (MessageType, MessageTitle, MessageContent, UserID, ToID) VALUES('$MessageType', '$MessageTitle', '$MessageContent', $UserID, $ToID)";
			// Execute
			$r = mysqli_query( $con, $q );


			// If the query was successful go ahead and return the class ID
			if ( $r ) {

				// Find out what the ClassID of thihs class is
				$q = "SELECT MessageID FROM Message ORDER BY MessageID DESC LIMIT 1";
				// Execute
				$r = mysqli_query( $con , $q );
				$row = mysqli_fetch_array( $r );
				$MessageID = $row["MessageID"];

				// echo the class id
				echo "{\"MessageID\" : $MessageID}";


				mysqli_close($con);

				exit;
			}
			else {
				errormsg("Invalid parameters given to create the message");
			}			

		}
		else {
			errormsg("Unrecognized Message Type");
		}
	}
	else {
		errormsg("Must be an instructor to create an area of focus.");
		exit;
	}


?>