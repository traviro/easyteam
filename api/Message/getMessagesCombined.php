<?php

// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.

	require_once('../php_includes/openDbConn.php');
	include('../php_includes/utility_functions.php');


	// We have to build the query sequentially because of all the optional Project input


	$UserID 	= clean("UserID", true, $con);
	$ProjectID 	= clean("ProjectID", true, $con);


	// Format the ending and take out the commas that shouldn't be there
	$q = "SELECT MessageID, Message.UserID, ToID, MessageType, MessageDate, MessageTitle, MessageContent, MessageSeen, UserName FROM Message, User WHERE ((Message.ToID=$UserID AND Message.MessageType='User') OR (Message.ToID=$ProjectID AND Message.MessageType='Project') OR (Message.MessageType='Global')) AND User.UserID=Message.UserID";
	$result = mysqli_query( $con, $q );


	// If the query was a success
	if( $result ) {
		// If there were results found
		if( mysqli_num_rows( $result) > 0) {


			$areas = array();

			while( $row = mysqli_fetch_array( $result ) ) {

				// Create a new array for this areaof focus
				$area = array(
					"MessageID" 	=> $row["MessageID"],
					"UserID" 		=> $row["UserID"],
					"ToID" 			=> $row["ToID"],
					"MessageType" 	=> $row["MessageType"],
					"MessageDate" 	=> $row["MessageDate"],
					"MessageTitle" 	=> $row["MessageTitle"],
					"MessageContent" => $row["MessageContent"],
					"MessageSeen" 	=> $row["MessageSeen"],
					"UserName"		=> $row["UserName"],
				);

				// Push it into the all classes array
				array_push($areas, $area);

			}

			// Echo our results
			echo json_encode($areas);

			// Close our connection and exit
			mysqli_close($con);

			exit;


		}
		else {
			errormsg("No messages found.");
		}
	}
	else {
		errormsg("Invalid ID specified.");
	}

?>