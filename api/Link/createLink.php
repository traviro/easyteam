<?php

// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.

	require_once('../php_includes/openDbConn.php');
	include('../php_includes/utility_functions.php');

	// Only instructors can create new areas of focus
	if( getUserType() == 3 ) {

		// Get and clean the variabels
		$URL = clean( "URL", true, $con);
		$LinkDescription = clean( "LinkDescription", true, $con);
		$PrimaryCategory = clean( "PrimaryCategory", true, $con);
	
		// Build query 
		$q = "INSERT INTO Link (URL, LinkDescription, PrimaryCategory) VALUES('$URL', '$LinkDescription', '$PrimaryCategory')";

		// Execute
		$r = mysqli_query( $con, $q );


		// If the query was successful go ahead and return the class ID
		if ( $r ) {

			// Find out what the ClassID of thihs class is
			$q = "SELECT LinkID FROM Link ORDER BY LinkID DESC LIMIT 1";
			// Execute
			$r = mysqli_query( $con , $q );
			$row = mysqli_fetch_array( $r );
			$LinkID = $row["LinkID"];

			// echo the class id
			echo "{\"LinkID\" : $LinkID}";


			mysqli_close($con);

			exit;
		}
		else {
			errormsg("Invalid parameters given to create the link");
		}			


	}
	else {
		errormsg("Must be an instructor to create the link");
		exit;
	}


?>