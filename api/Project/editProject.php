<?php

// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.

	require_once('../php_includes/openDbConn.php');
	include('../php_includes/utility_functions.php');


	if(isset( $_GET["ProjectID"] )) {

		// Any logged in Users can edit the projects. This may be changed later but for the sake of time that's the policy
		if( getUserType() > 0) {

			// We have to build the query sequentially because of all the optional Project input

			$values = "";

			if( isset( $_GET["ClassID"] )) {
				$values .= "ClassID=" . clean("ClassID", true, $con) . ", ";
			} 

			if( isset( $_GET["ProjectID"] )) {
				$values .= "ProjectID=".  clean("ProjectID", true, $con) . ", ";
			} 

			if( isset( $_GET["SponsorID"] )) {
				$values .= "SponsorID=" .  clean("SponsorID", true, $con) . ", ";
			} 

			if( isset( $_GET["AdvocateID"] )) {
				$values .= "AdvocateID='" . clean("AdvocateID", true, $con) . "', ";
			} 

			if( isset( $_GET["ProjectName"] )) {
				$values .= "ProjectName='" . clean("ProjectName", true, $con) . "', ";
			} 

			if( isset( $_GET["Description"] )) {
				$values .= "Description='" . clean("Description", true, $con) . "', ";
			} 		

			// clean the ProjectID
			$ProjectID = clean("ProjectID", true, $con);	

			// Format the ending and take out the commas that shouldn't be there
			if( $values != "") {
				$values = substr($values, 0, strlen($values) - 2);
			}
			else {
				$values = "ProjectID=$ProjectID";;
			}	

			// Now we can finally build the query
			$q = "UPDATE Project SET " . $values . " WHERE ProjectID=$ProjectID";
			// Exectue
			$r = mysqli_query( $con, $q );

			// If the Project creation request completed successfully, lets find out what the ProjectID is and return it.
			if( $r ) {

				// Find out what the ProjectID of thihs class is
				$q = "SELECT ProjectID FROM Project ORDER BY ProjectID DESC LIMIT 1";
				// Execute
				$r = mysqli_query( $con , $q );
				$row = mysqli_fetch_array( $r );
				$ProjectID = $row["ProjectID"];


				// echo the class id
				echo "{\"Success\" : true}";
				
			}
			// Otherwise we show an error.
			else {
				errormsg("Unable to edit Project. Make sure all fields were filled out correctly.");
			}
		}
		else {
			errormsg("You do not have permission to edit this Project.");
		}
	}
	else {
		errormsg("No ProjectID was specified.");
	}


?>