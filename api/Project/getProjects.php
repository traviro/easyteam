<?php

// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.

	require_once('../php_includes/openDbConn.php');
	include('../php_includes/utility_functions.php');


	// We have to build the query sequentially because of all the optional Project input
	$values = "";

	if( isset( $_GET["ProjectID"] )) {
		$values .= "ProjectID=" . clean("ProjectID", true, $con) . " AND ";
	} 

	if( isset( $_GET["ClassID"] )) {
		$values .= "ClassID=" . clean("ClassID", true, $con) . " AND ";
	} 

	if( isset( $_GET["SponsorID"] )) {
		$values .= "SponsorID=".  clean("SponsorID", true, $con) . " AND ";
	} 

	if( isset( $_GET["AdvocateID"] )) {
		$values .= "AdvocateID=" .  clean("AdvocateID", true, $con) . " AND ";
	} 

	if( isset( $_GET["ProjectName"] )) {
		$values .= "ProjectName='" . clean("ProjectName", true, $con) . "' AND ";
	} 

	if( isset( $_GET["Description"] )) {
		$values .= "Description='" . clean("Description", true, $con) . "' AND ";
	} 

	// Format the ending and take out the commas that shouldn't be there
	if( $values != "") {
		$values = substr($values, 0, strlen($values) - 5);
		$q = "SELECT * FROM Project WHERE " . $values;
	}
	else {
		$q = "Select * FROM Project";
	}	
	$result = mysqli_query( $con, $q );


	if( $result ) {
		// Initialize classes array
		$allProjects = array();

		while( $row = mysqli_fetch_array( $result ) ) {

			// Create a new array for this class
			$Project = array(
				"ProjectID" => $row["ProjectID"],
				"ClassID" => $row["ClassID"],
				"SponsorID" => $row["SponsorID"],
				"AdvocateID" => $row["AdvocateID"],
				"ProjectName" => $row["ProjectName"],
				"ProjectDate" => $row["ProjectDate"],
				"Description" => $row["Description"],
			);

			// Push it into the all classes array
			array_push($allProjects, $Project);

		}

		// Echo our results
		echo json_encode($allProjects);

		// Close our connection and exit
		mysqli_close($con);

		exit;
	}
	else {
		errormsg("Invalid search parameters.");
	}



?>