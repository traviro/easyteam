<?php

// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.

	require_once('../php_includes/openDbConn.php');
	include('../php_includes/utility_functions.php');


	// Only instructors can create Projects
	if( getUserType() == 3 ) {


		// We have to build the query sequentially because of all the optional Project input

		$colNames = "(";
		$values = "(";

		if( isset( $_GET["ClassID"] )) {
			$colNames .= "ClassID, ";
			$values .= clean("ClassID", true, $con) . ", ";
		} 

		if( isset( $_GET["SponsorID"] )) {
			$colNames .= "SponsorID, ";
			$values .= clean("SponsorID", true, $con) . ", ";
		} 

		if( isset( $_GET["AdvocateID"] )) {
			$colNames .= "AdvocateID, ";
			$values .= clean("AdvocateID", true, $con) . ", ";
		} 


		if( isset( $_GET["ProjectName"] )) {
			$colNames .= "ProjectName, ";
			$values .= "'" . clean("ProjectName", true, $con) . "', ";
		} 	

		if( isset( $_GET["Description"] )) {
			$colNames .= "Description, ";
			$values .= "'" . clean("Description", true, $con) . "', ";
		} 		


		// Format the ending and take out the commas that shouldn't be there
		if( $colNames != "(" && $values != "(") {
			$colNames = substr($colNames, 0, strlen($colNames) - 2) . ")";
			$values = substr($values, 0, strlen($values) - 2) . ")";
		}
		else {
			$colNames = "()";
			$values = "()";
		}			



		// Now we can finally build the query
		$q = "INSERT INTO Project " . $colNames . " VALUES " . $values;
		// Exectue
		$r = mysqli_query( $con, $q );

		// If the Project creation request completed successfully, lets find out what the ProjectID is and return it.
		if( $r ) {

			// Find out what the ProjectID of thihs class is
			$q = "SELECT ProjectID FROM Project ORDER BY ProjectID DESC LIMIT 1";
			// Execute
			$r = mysqli_query( $con , $q );
			$row = mysqli_fetch_array( $r );
			$ProjectID = $row["ProjectID"];


			// echo the class id
			echo "{\"ProjectID\" : $ProjectID}";

		}
		// Otherwise we show an error.
		else {
			errormsg("Unable to create project. Check to make sure all fields were entered correctly.");
		}



	}
	else {
		errormsg("You do not have permission to create projects.");
	}


?>