<?php

// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.

	require_once('../php_includes/openDbConn.php');
	include('../php_includes/utility_functions.php');



	// Only instructors can delete projects
	if( getUserType() == 3 ) {

		$ProjectID = clean( "ProjectID", true, $con );



		// First let's remove all references to this project in the user table and such
		$q = 'UPDATE User SET ProjectID=null WHERE ProjectID=$ProjectID';
		$result = mysqli_query($con, $q);



		// Now remove the actual project entry
		// Build query
		$q = "DELETE FROM Project WHERE ProjectID=$ProjectID";
		// Execute
		$result = mysqli_query($con, $q);


		// Check to make sure the query went through
		if( $result )
			echo "{\"Success\" : true }";
		else
			errormsg("Invalid ProjectID provided");

	}
	else {
		errormsg("Must be an instructor to delete a Project.");

		exit;
	}	

?>