<?php

// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.

	require_once('../php_includes/openDbConn.php');
	include('../php_includes/utility_functions.php');

	// Only instructors can create new areas of focus
	if( getUserType() >= 0 ) {

		// Get and clean the variabels
		$ApplicationPath = clean( "ApplicationPath", true, $con);
		$ProjectID 			= clean( "ProjectID", true, $con);
		$UserID		= getUserID();


		// first check if appplication already exist
		$q = "SELECT ProjectApplicationID FROM ProjectApplication WHERE ProjectID=$ProjectID AND UserID=$UserID";
		$r = mysqli_query( $con, $q );
		if(mysqli_num_rows($r) > 0 ) {

			$q = "UPDATE ProjectApplication SET ApplicationPath=$ApplicationPath WHERE ProjectID=$ProjectID AND UserID=$UserID";
		}
		else {
			// Build query 
			$q = "INSERT INTO ProjectApplication (ProjectID, UserID, ApplicationPath) VALUES($ProjectID, $UserID, '$ApplicationPath')";
		}

		// Execute
		$r = mysqli_query( $con, $q );


		// If the query was successful go ahead and return the class ID
		if ( $r ) {

			// Find out what the ClassID of thihs class is
			$q = "SELECT ProjectApplicationID FROM ProjectApplication ORDER BY ProjectApplicationID DESC LIMIT 1";
			// Execute
			$r = mysqli_query( $con , $q );
			$row = mysqli_fetch_array( $r );
			$ProjectApplicationID = $row["ProjectApplicationID"];

			// echo the class id
			echo "{\"ProjectApplicationID\" : $ProjectApplicationID}";


			mysqli_close($con);

			exit;
		}
		else {
			errormsg("Invalid parameters given to create the application");
		}			


	}
	else {
		errormsg("Must be an instructor to create an area of focus.");
		exit;
	}


?>