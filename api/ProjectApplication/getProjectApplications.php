<?php

// Copyright (c) 2013 All Right Reserved, EasyTeam easyteam411@gmail.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.

	require_once('../php_includes/openDbConn.php');
	include('../php_includes/utility_functions.php');


	// We have to build the query sequentially because of all the optional Project input
	$values = "";

	if( isset( $_GET["ProjectApplicationID"] )) {
		$values .= "ProjectApplication.ProjectApplicationID=" . clean("ProjectApplicationID", true, $con) . " AND ";
	} 

	if( isset( $_GET["UserID"] )) {
		$values .= "ProjectApplication.UserID=" . clean("UserID", true, $con) . " AND ";
	} 

	if( isset( $_GET["ProjectID"] )) {
		$values .= "ProjectApplication.ProjectID=" . clean("ProjectID", true, $con) . " AND ";
	} 	

	if( isset( $_GET["ApplicationStatus"] )) {
		$values .= "ProjectApplication.ApplicationStatus=" . clean("ApplicationStatus", true, $con) . " AND ";
	} 	

	if( isset( $_GET["ApplicationDate"] )) {
		$values .= "ProjectApplication.ApplicationDate='".  clean("ApplicationDate", true, $con) . "' AND ";
	} 

	if( isset( $_GET["ApplicationPath"] )) {
		$values .= "ProjectApplication.ApplicationPath='" .  clean("ApplicationPath", true, $con) . "' AND ";
	} 




	// Format the ending and take out the commas that shouldn't be there
	if( $values != "") {
		$values = substr($values, 0, strlen($values) - 5);
		$q = "SELECT * FROM ProjectApplication WHERE " . $values;
	}
	else {
		$q = "Select * FROM ProjectApplication";
	}	
	$result = mysqli_query( $con, $q );


$q = 'SELECT * FROM ProjectApplication JOIN ProjectRating ON ProjectApplication.ProjectID=ProjectRating.ProjectID JOIN User ON ProjectApplication.UserID=User.UserID WHERE User.UserID=ProjectApplication.UserID AND User.UserID=ProjectRating.UserID AND ' . $values;


/*

	$q = "SELECT ProjectApplication.ProjectApplicationID, ProjectApplication.ProjectID, ProjectApplication.UserID, ProjectApplication.ApplicationDate, ProjectApplication.ApplicationPath, ProjectApplication.ApplicationStatus, User.UserName, ProjectRating.RatingVal FROM ProjectApplication, ProjectRating, User WHERE ".$values." AND ProjectRating.ProjectID=ProjectApplication.ProjectID AND ProjectRating.UserID=User.UserID ORDER BY ProjectRating.RatingVal ASC";
*/

	// Execute
	$result = mysqli_query($con, $q);

	// If the query was a success
	if( $result ) {
		// If there were results found
		if( mysqli_num_rows( $result) > 0) {


			$areas = array();

			while( $row = mysqli_fetch_array( $result ) ) {

				// Create a new array for this areaof focus
				$area = array(
					"ProjectApplicationID" 	=> $row["ProjectApplicationID"],
					"ProjectID" 		=> $row["ProjectID"],
					"UserID" 			=> $row["UserID"],
					"ApplicationDate" 	=> $row["ApplicationDate"],
					"ApplicationPath" 	=> $row["ApplicationPath"],
					"ApplicationStatus" => $row["ApplicationStatus"],
					"UserName"			=>	$row['UserName'],
					"RatingVal"			=> $row['RatingVal'],
				);

				// Push it into the all classes array
				array_push($areas, $area);

			}

			// Echo our results
			echo json_encode($areas);

			// Close our connection and exit
			mysqli_close($con);

			exit;


		}
		else {
			errormsg("No applications found.");
		}
	}
	else {
		errormsg("Invalid ID specified.");
	}

?>